# Running NBOS Using Docker
## Installation/Configuration
**Installing Necessary Software**
To run the EDC locally, first make sure you have installed the following software:
-   [Docker Desktop](https://www.docker.com/products/docker-desktop)

**Adding Relevant Configuration Files**
There is one configuration file `shiro.ini` that, for security reasons, is not committed to git. You must add these manually. You can find this file on the `hsdev` servers in the `/etc/ecp` directory. If you are struggling copying this file over from one of the hsdev servers, contact someone on the HS EDC team. When you get access to this file, copy it into the `/docker/web/ecp` folder of the HS EDC repository. This file has been added to the `.gitignore` so changes to it will not be tracked.
## Running the EDC Using Docker
For reference, in the past making and viewing changes to the EDC application involved building a `.war` file and deploying it to a server which had a running instance of Tomcat and Postgres. There were two major problems with this approach:

-   Deploying the `.war` file took a couple minutes.
-   Only one person could be working at a time.

To alleviate these problems, you can now develop the EDC locally on your machine by using Docker.

To instantiate the Docker containers needed to view the  EDC locally, all you need to do is run the command `docker-compose up` in your terminal in the  EDC repository. Note: you must be at the same level as the `docker-compose.yml` file when you run this command.

Running this command will cause your terminal to "attach" to the two created containers. This means that you will see the logs of the containers in real time. If you do not want this behavior, run `docker-compose run -d` to run the containers in detached mode.

Once the containers are running, you can navigate to `localhost:8080/CTT/` to view rbos.

**Common Errors**
-  If you can't login, make sure you are connected to the VPN. In order for the application to verify your LDAP credentials, you must be connected to the VPN.
-  If you are getting a "cannot find a resource at this location error", make sure that you have added the `shiro.ini` file as described above. 

## Docker Development Workflow
**Viewing Changes**

Once the Docker containers are up and running, any time you build the project, changes will be automatically deployed into the containers, although sometimes this can take a couple seconds. You can look at the logs of the containers to see when the new war file has been deployed.

-   *Note: If you are using Windows, it looks like due to the type of filemount, changes aren't deploying automatically and results in an error with the container. So, every time you build the project, you will have to restart the Tomcat container. This can be done easily using the Docker Desktop application. Find the web container and click the `restart` button.*

**Stopping and Removing Docker Containers**

If you are attached to the containers (you can view the logs in your terminal), you can stop the containers by using `control` + `c`. If you are not attached, you can use the command `docker-compose stop`.

Stopping the containers means that all data will be persisted once you restart the containers. To restart the containers run `docker-compose start`.

If you you would like to completely remove containers, run `docker-compose down`. This will erase all data written to the database so that when you run `docker-compose up` you will start with a clean version of the database.

**Other Useful Docker Commands**

`docker ps` — View all running containers. This is how you find out the container id.

`docker logs <container id>` — View the logs of a container

`docker exec -it <container id> /bin/bash` — Start a bash shell inside a container. This is particularly useful for debugging.

## Notes about the Postgres Database Container
**Accessing the Database**

By default, the docker container running the postgres database is mapping to your `localhost:5432`. If you would like to connect to the database, you can do so by connecting to the database at this address.
**Making Changes to the Initialization Script**
If you would like to change the SQL script that is run to initiate the database, you can do so by providing overwriting the `/docker/db/output.sql` file. To generate this file from running postgres instance, you can run the command:
```
pg_dump -d <DATABASE NAME> -U <USERNAME> -W -x -f output.sql
```

## Known Issues
Right now, the database is initialized with very little data inside of it. This might cause problems when loading certain pages. We will want to decide on a script that will initialize the database with some amount of data inside of it. 
