[go back](/README.md)

#Filled Workbook Documentation Apache POI

##Class Overview:
* Can create preset spreadsheets and pass in dynamic data into cells
* Constructor contains a switch case to determine which preset to use
<br /> <br /> 
* Constructor
    * Takes in string representing preset (must match a case)
    * Instantiates workbook and cols list
      <br /> <br /> 
* Current Presets
  * Enrollment Projection Report (AKA George Report)
    * generateGeorge()
    * Has custom row labels (year of enrollment)
    * Custom column labels (site name)
    * Dynamically fills remaining rows
      <br /> <br />
* publishFile()
  * Exact same as the publishFile from the ResultSetWorkBook, but requires a path input
    <br />  <br />    
* Adding a new preset
    1. Add a new case to the switch case in your constructor
    2. Create a new sheet (wb.createSheet(name))
    3. Create a new method
    4. Use existing publishFile method OR create a new one if needed.
       <br /> <br />     
* Helpful methods and other tips
  * sheet.createRow (index)
    * Creates and returns a new row in the sheet at the specified location
    * Can call createCell(col) to create a cell at a particular column.
  * cell.setCellValue(value)
    * Can add information to cells