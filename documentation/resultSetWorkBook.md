[go back](/README.md)
#Result Set Workbook Documentation Apache POI


##Class Overview:
* Takes in result set as raw input and converts it into an excel workbook  <br />
* Resulting workbook will contain a single sheet
    * Columns will be labeled based on the columns returned in the query
    * Cannot create row labels with this class
      <br />  <br />
* Constructor
    * Takes in result set and resulting file path on the server
    * Ensure that the nrg-svc-... user has access to this file path
    * Also instantiates the logger, sheet object, workbook object and column list
    * Also runs generateCols()
      <br /> <br />
* Important Methods
    * generateCols()
        * Uses the result set meta data to return info about the columns returned by the query
        * Saves the column names in the cols instance variable
    * fillRows()
        * Must be called externally as of now
        * Loops through the entire result set and copies the different objects into the necessary rows
        * We need to use Objects.toString when getting the value, because the query may return nonstring values (like a Date)
    * publishFile()
        * Loops through columns and auto-fits them (all rows in a column become the size of the largest row in the column)
        * Writes the workbook to an EXISTING xlsx file
            * **Note that an existing holder xlsx file must already exist at the specified path**
        * Sets published instance variable to true
            * This variable ensures that no changes are made to the workbook after it has been published
    
