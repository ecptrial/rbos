package edu.wustl.mir.ctt.beans;

import edu.wustl.mir.ctt.Controller;
import edu.wustl.mir.ctt.ReportsController;
import edu.wustl.mir.ctt.SecurityManager;
import edu.wustl.mir.ctt.form.BasicForm;
import edu.wustl.mir.ctt.model.*;
import edu.wustl.mir.ctt.notification.NotificationException;
import edu.wustl.mir.ctt.persistence.PersistenceException;
import edu.wustl.mir.ctt.persistence.PersistenceManager;
import edu.wustl.mir.ctt.persistence.ServiceRegistry;
import org.primefaces.context.RequestContext;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import edu.wustl.mir.ctt.log.AuditLogger;

/**
 *
 * @author drm
 */
@ManagedBean
@RequestScoped
public class AddEventBean implements Serializable {
    private ECPEventTypes eventType;
    private Date dateOfOccurance;
    private String title;
    
     private AuditLogger logger = AuditLogger.create(ReportsController.class);
    
    @ManagedProperty(value="#{controller}")
    private Controller controller;
    
    public AddEventBean() {
    }
    
    public List<ECPEventTypes> getEventTypes() {
        StudyArmStatus sas = controller.getSelectedParticipant().getStudyArmStatus();
        List<ECPEventTypes> eventTypes;
        
        if("Observational Arm".equals(sas.getName())){
            eventTypes = ECPEventTypes.getObservationalArmUnscheduledEvents();
        } else {
            eventTypes = ECPEventTypes.getUnscheduledEvents();
        }
        
        if (eventTypes.contains(ECPEventTypes.CHANGE_THERAPY) && !canAddChangeInTherapyForm()) {
            eventTypes = new ArrayList(eventTypes);
            eventTypes.remove(ECPEventTypes.CHANGE_THERAPY);
        }
        
        if (eventTypes.contains(ECPEventTypes.PARTICIPANT_FOLLOW_UP) && !canAddParticipantFollowUpForm()) {
            eventTypes = new ArrayList(eventTypes);
            eventTypes.remove(ECPEventTypes.PARTICIPANT_FOLLOW_UP);
        }

     /*   if (eventTypes.contains(ECPEventTypes.PULMONARY_EVAL) && !canAddPulmonaryEvalForm()){
            eventTypes = new ArrayList<>(eventTypes);
            eventTypes.remove(ECPEventTypes.PULMONARY_EVAL);
        }
        */
        return eventTypes;
    }

    public ECPEventTypes getEventType() {
        return eventType;
    }

    public void setEventType(ECPEventTypes eventType) {
        this.eventType = eventType;
    }
    
    public String addEventAction() throws PersistenceException, NotificationException {
        // NOTE: If the event happens to be a Change in Therapy, the Change in Therapy form does not get populated with a copy of most of the baseline therapy values
        // or the Change in Therapy until the Change in Therapy form is read (See JDBCPersistenceManager.getChangeTherapyForm for details relating to copying the Baseline
        // or Change values to the newly created Change in Therapy form).
        Event e = ECPEvents.getInstance(eventType);
        logger.audit("The addEventAction eventType is: " + eventType.getName() + "  " + eventType.name());
        e.setActualDate( dateOfOccurance);

        // Prevent anyone but technical coordinators from creating more than one pulmonary evaluation
        if (!SecurityManager.canVerifyForms() && e.getType() == ECPEventTypes.PULMONARY_EVAL && !canAddPulmonaryEvalForm()){
            RequestContext.getCurrentInstance().execute("PF('pulEvalFailed').show()");
            return null;
        }
        if (!SecurityManager.canVerifyForms() && e.getType() == ECPEventTypes.ECP_TREATMENT && !canAddECPForm()){
            RequestContext.getCurrentInstance().execute("PF('ECPFailed').show()");
            return null;
        }
        return controller.insertEventAction( e, dateOfOccurance);
    }

    public Date getDateOfOccurance() {
        return dateOfOccurance;
    }

    public void setDateOfOccurance(Date dateOfOccurance) {
        this.dateOfOccurance = dateOfOccurance;
    }
    
    public void setController(Controller c) {
        this.controller = c;
    }
    
    public Controller getController() {
        return controller;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean canAddChangeInTherapyForm() {
        boolean canAddChangeInTherapyForm = false;
        Participant p = controller.getSelectedParticipant();
        Site s = controller.getSelectedSite();
        
        //Only participants enrolled before IRB version 7.1 can add Change in Therapy,
        //after that it became a scheduled form rather than an unscheduled one.
        Calendar followUpThreshold = new GregorianCalendar(2017, 12, 1);
        
        if (p.getEnrolledDate().before(followUpThreshold.getTime())) {
            canAddChangeInTherapyForm = true;
        }
        
        return canAddChangeInTherapyForm;
    }
    
    public boolean canAddParticipantFollowUpForm() {
        boolean canAddParticipantFollowUpForm = true;
        Participant p = controller.getSelectedParticipant();
        Site s = controller.getSelectedSite();
        
        Calendar followUpThreshold = new GregorianCalendar(2017, 12, 1);
        
        if (s.getIrbVersion().equals("8.0") && p.getEnrolledDate().before(followUpThreshold.getTime())) {
            try {
                PersistenceManager pm = ServiceRegistry.getPersistenceManager();

                List<Event> le = pm.getEvents(p);

                // Only allow one participant follow-up form
                for (Event e : le) {
                    if (e.getType() == ECPEventTypes.PARTICIPANT_FOLLOW_UP) {
                        canAddParticipantFollowUpForm = false;
                    }
                }
            } catch (PersistenceException pe) {
                // fail
            }
        } else {
            canAddParticipantFollowUpForm = false;
        }
        
        return canAddParticipantFollowUpForm;
    }
    
    public boolean canAddPulmonaryEvalForm() {
        boolean canAddPulmonaryEvalForm = true;
        Participant p = controller.getSelectedParticipant();

            try {
                PersistenceManager pm = ServiceRegistry.getPersistenceManager();

                List<Event> le = pm.getEvents(p);

                // Only allow one participant follow-up form
                for (Event e : le) {
                    if (e.getType() == ECPEventTypes.PULMONARY_EVAL && e.getStatus() == EventStatus.NEW && !e.getName().contains(":")) {
                        canAddPulmonaryEvalForm = false;
                    }
                }
            } catch (PersistenceException pe) {
                // fail
            }

        return canAddPulmonaryEvalForm;
    }

    public boolean canAddECPForm() {
        boolean canAddECPForm = true;
        Participant p = controller.getSelectedParticipant();

        try {
            PersistenceManager pm = ServiceRegistry.getPersistenceManager();

            List<Event> le = pm.getEvents(p);

            // Only allow one participant follow-up form
            for (Event e : le) {
                if (e.getType() == ECPEventTypes.ECP_TREATMENT && e.getStatus() == EventStatus.NEW && !e.getName().contains(":")) {
                    canAddECPForm = false;
                }
            }
        } catch (PersistenceException pe) {
            // fail
        }

        return canAddECPForm;
    }
}
