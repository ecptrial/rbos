package edu.wustl.mir.ctt.beans;

import edu.wustl.mir.ctt.AEController;
import edu.wustl.mir.ctt.Controller;
import edu.wustl.mir.ctt.form.AdverseEventWorksheetSAEForm;
import edu.wustl.mir.ctt.form.BasicForm;
import edu.wustl.mir.ctt.model.ECPEventTypes;
import edu.wustl.mir.ctt.model.ECPEvents;
import edu.wustl.mir.ctt.model.ECPFormTypes;
import edu.wustl.mir.ctt.model.Event;
import edu.wustl.mir.ctt.model.EventStatus;
import edu.wustl.mir.ctt.model.FormStatus;
import edu.wustl.mir.ctt.notification.NotificationException;
import edu.wustl.mir.ctt.persistence.PersistenceException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author drm
 */
@ManagedBean
@RequestScoped
public class AddFormBean implements Serializable {
    private Event event;
    private ECPFormTypes formType;
    private Date dateOfOccurance;
    private String title;
    
    @ManagedProperty(value="#{controller}")
    private Controller controller;
    
    public AddFormBean() {
    }
    
    @PostConstruct
    public void init() {
        // Automatically put today's date into the Add SAE Follow-up Form popup window.
        dateOfOccurance = new Date();
        event = controller.getSelectedEvent();
    }
    
    
    public List<ECPFormTypes> getFormTypes() {
        return event.getType().getUnscheduledFormTypes();
    }

    public ECPFormTypes getFormType() {
        return formType;
    }

    public void setFormType(ECPFormTypes formType) {
        this.formType = formType;
    }
    
    public String addFormAction() throws PersistenceException, NotificationException {
        BasicForm form = ECPFormTypes.getInstance(formType, event);
        form.setDate(dateOfOccurance);
        // Set the current date to be the same as the Form Date (dateOfOccurrance) found in the Add SAE Follow-up Form popup window.
        AdverseEventWorksheetSAEForm aews = (AdverseEventWorksheetSAEForm) form;
        aews.setCurrentDate(dateOfOccurance);
        // The newly created form needs to be inserted into the database before the event status can be updated.
        String eventSummaryPage = controller.insertFormAction( event, aews);
        // If you try to update the event status before inserting the new form, the status will be only from the
        // previously created forms.  The idea is to update the event status to be NEW when a new form is added.
        // The updateEventStatus gets the forms from the database for the particular event passed to it.  The 
        // status of each form is checked to determine the lowest status precedence, and the lowest status is 
        // used to determine the event status.
        controller.updateEventStatus(event);
        
        return eventSummaryPage;
    }

    public Date getDateOfOccurance() {
        return dateOfOccurance;
    }

    public void setDateOfOccurance(Date dateOfOccurance) {
        this.dateOfOccurance = dateOfOccurance;
    }
    
    public void setController(Controller c) {
        this.controller = c;
    }
    
    public Controller getController() {
        return controller;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
