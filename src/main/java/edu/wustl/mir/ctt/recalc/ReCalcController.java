package edu.wustl.mir.ctt.recalc;

import edu.wustl.mir.ctt.calc.CrossoverEligibilityCalculator;
import edu.wustl.mir.ctt.calc.StudyArmEligibilityCalculator;
import edu.wustl.mir.ctt.directory.DirectoryManager;
import edu.wustl.mir.ctt.directory.DirectoryManagerException;
import edu.wustl.mir.ctt.form.BasicForm;
import edu.wustl.mir.ctt.form.EligibilityForm;
import edu.wustl.mir.ctt.form.PulmEvalForm;
import edu.wustl.mir.ctt.model.ECPEventTypes;
import edu.wustl.mir.ctt.model.ECPFormTypes;
import edu.wustl.mir.ctt.model.Event;
import edu.wustl.mir.ctt.model.EventStatus;
import edu.wustl.mir.ctt.model.Participant;
import edu.wustl.mir.ctt.model.PulmonaryEvaluation;
import edu.wustl.mir.ctt.model.Site;
import edu.wustl.mir.ctt.model.StudyArmStatus;
import edu.wustl.mir.ctt.persistence.PersistenceException;
import edu.wustl.mir.ctt.persistence.PersistenceManager;
import edu.wustl.mir.ctt.persistence.ServiceRegistry;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.event.AjaxBehaviorEvent;
import org.apache.logging.log4j.LogManager;
import edu.wustl.mir.ctt.log.AuditLogger;

@ManagedBean
@SessionScoped
public class ReCalcController implements Serializable {
//    private StudyArmEligibilityForm eform;
    private StudyArmEligibilityCalculator calc;
    private CrossoverEligibilityCalculator crossCalc;
    private Site site;
    //private final Site noSite;  // use this site when "None" is selected in dropdown.
    private final List<Site> sites;
    private final Converter siteConverter;
    private Participant participant;
    private final Participant noParticipant;  // use this participant when "None" is selected in dropdown.
    private List<Participant> participants;
    private List<Participant> obsParticipants;
    private final Converter participantConverter;
    // the list of evaluations used for form I/O.
    private List<PulmonaryEvaluation> inputEvaluations;
    private Date referenceDate;
    private Site defaultSite;
    
    // the list of inputEvaluations with data (non-null contents).
    // this list is passed to the calculator.
    private List<PulmonaryEvaluation> activeEvaluations;
    private boolean renderChart;
    
    private transient AuditLogger logger = AuditLogger.create(ReCalcController.class);
    
    public ReCalcController() throws PersistenceException, DirectoryManagerException {
        renderChart = false;
        
        inputEvaluations = new ArrayList<PulmonaryEvaluation>();
        inputEvaluations.add( new PulmonaryEvaluation( null, null, null));
        inputEvaluations.add( new PulmonaryEvaluation( null, null, null));
        inputEvaluations.add( new PulmonaryEvaluation( null, null, null));
        inputEvaluations.add( new PulmonaryEvaluation( null, null, null));
        inputEvaluations.add( new PulmonaryEvaluation( null, null, null));
        inputEvaluations.add( new PulmonaryEvaluation( null, null, null));
        inputEvaluations.add( new PulmonaryEvaluation( null, null, null));
        inputEvaluations.add( new PulmonaryEvaluation( null, null, null));
        inputEvaluations.add( new PulmonaryEvaluation( null, null, null));
        inputEvaluations.add( new PulmonaryEvaluation( null, null, null));
        
        PersistenceManager pm = ServiceRegistry.getPersistenceManager();
        sites = pm.getSites();
        
        
        
        defaultSite = null;
        
        DirectoryManager dm = ServiceRegistry.getDirectoryManager();
        String userDefaultSite = dm.getSiteName();

        for (Site s : sites) {
            if( s.getName().equals(userDefaultSite)) {
                defaultSite = s;
                site = s;
                break;
            }
        }
        
        if (defaultSite == null) {
            defaultSite = new Site();
            defaultSite.setName("No Site Selected");
        }
        
        //noSite = new Site();
        //noSite.setName("None");
        //sites.add(noSite);
        //site = noSite;
        
        siteConverter = new SiteConverter();
        referenceDate = new Date();

        if ("No Site Selected".equals(defaultSite.getName())) {
            participants = new ArrayList<>();
            obsParticipants = new ArrayList<>();
         } else {
            participants = pm.getParticipants(defaultSite);
            
            obsParticipants = new ArrayList<>();
            for (Participant p : participants) {
                if (p.getStudyArmStatus().equals(StudyArmStatus.OBSERVATIONAL_ARM)) {
                    obsParticipants.add(p);
                }
            }
        }
        
        noParticipant = new Participant();
        noParticipant.setParticipantID("New Participant");
        participants.add(noParticipant);
        obsParticipants.add(noParticipant);
        participant = noParticipant;
        participantConverter = new ParticipantConverter();
    }
    
    private void clearEvaluations() {
        for( PulmonaryEvaluation e: inputEvaluations) {
            e.setDate(null);
            e.setFev1(null);
            e.setFvc(null);
        }
    }
    
    private void clearResults() {
        calc = null;
        crossCalc = null;
    }
    
    private void clearChart() {
        renderChart = false;
    }
    
    public String resetCalculatorAction() {
        site = defaultSite;
        participant = noParticipant;
        referenceDate = new Date();
        
        renderChart = false;
        
        clearEvaluations();
        
        calc = null;
        crossCalc = null;
        
        // stay on same page
        return null;
    }
    
    public String moreEvalsAction() {
        inputEvaluations.add( new PulmonaryEvaluation( null, null, null));
        inputEvaluations.add( new PulmonaryEvaluation( null, null, null));
        inputEvaluations.add( new PulmonaryEvaluation( null, null, null));
        
        // stay on same page
        return null;
        //return "/recalc/calculator.xhtml";
    }
    
    public String eligibilityCalcAction() {
        activeEvaluations = initActiveEvaluations();
        
        /*if( activeEvaluations.size() < 5) {
            FacesContext ctx = FacesContext.getCurrentInstance();
            ctx.addMessage(null, new FacesMessage("Submission Error:", "At least five measurements are required for calculation."));
            // return null so browser stays on form that failed to submit.
            // forms will detect failed submit and display warning.
            return null;
        }
        calc = new StudyArmEligibilityCalculator( activeEvaluations, referenceDate, false, site);
        // Do not rencer the chart if there are less than 5 valid FEV1s
        if(calc.isDataTooFew() || calc.isDataStale()) {
            renderChart = false;
            FacesContext ctx = FacesContext.getCurrentInstance();
            ctx.addMessage(null, new FacesMessage("Submission Error:", "At least five measurements spaced at least fourteen days apart within the six months before the reference date are required, and the most recent measurement must be within fourteen days prior to the reference date."));
            // return null so browser stays on form that failed to submit.
            // forms will detect failed submit and display warning.
            return null;
        } else */
        calc =  new StudyArmEligibilityCalculator( activeEvaluations, referenceDate, false, site);
        
        if (!calc.isStudyEligible()) {
            renderChart = false;
        } else {
            renderChart = true;
        }
        
        // stay on same page
        return null;
       // return "/recalc/calculator.xhtml";
    }
    
    public String crossoverCalcAction() {
        activeEvaluations = initActiveEvaluations();
        
        crossCalc = new CrossoverEligibilityCalculator(activeEvaluations, referenceDate, participant, site);
        calc = crossCalc.getCalculator();
        
       // Do not rencer the chart if there are less than 5 valid FEV1s
        //if(crossCalc.isDataTooFew() || crossCalc.isDataStale()) {
        if (crossCalc.isCrossoverEligible() || crossCalc.isOverrideEligible()) {
            renderChart = true;
        } else {
            renderChart = true;
        }
        
        // stay on same page
        return null;
       // return "/recalc/calculator.xhtml";
    }

    public String registryAction() {
        // stay on same page
        return null;
        // return "/recalc/calculator.xhtml";
    }

//    public StudyArmEligibilityForm getEligibilityForm() {
//        return eform;
//    }
//
//    public void setEligibilityForm( StudyArmEligibilityForm eform) {
//        this.eform = eform;
//    }

    public StudyArmEligibilityCalculator getCalculator() {
        return calc;
    }

    public void setCalculator(StudyArmEligibilityCalculator calc) {
        this.calc = calc;
    }
    
    public CrossoverEligibilityCalculator getCrossoverCalculator() {
        return crossCalc;
    }
    
    public void setCrossoverCalculator(CrossoverEligibilityCalculator c) {
        this.crossCalc = c;
    }

    public List<PulmonaryEvaluation> getInputEvaluations() {
//       System.out.println("I am in the getInputEvaluations method!!!!!!!!!!!!!!!");
//        return eform.getPulmEvaluations();
        return inputEvaluations;
    }
    
    public List<PulmonaryEvaluation> getActiveEvaluations() {
        return activeEvaluations;
    }
    
    public List<PulmonaryEvaluation> getEvalsWithQualifyingDates() {
        List<PulmonaryEvaluation> qualified;
        
        if (calc != null) {
            qualified = calc.getEvalsWithQualifyingDates();
        } else if (crossCalc != null) {
            qualified = crossCalc.getEvalsWithQualifyingDates();
        } else {
            qualified = new ArrayList<PulmonaryEvaluation>();
        }
        
        return qualified;
    }
    
    public List<Site> getSites() {
        return sites;
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }
    
    public Date getReferenceDate() {
        return referenceDate;
    }
    
    public void setReferenceDate(Date date) {
        this.referenceDate = date;
    }
    
    private List<PulmonaryEvaluation> initActiveEvaluations() {
        List<PulmonaryEvaluation> evals = new ArrayList<PulmonaryEvaluation>();
        
        for( PulmonaryEvaluation e: inputEvaluations) {
            Date d = e.getDate();
            Float fev = e.getFev1();
            if( d != null && fev != null) {
                PulmonaryEvaluation newe = new PulmonaryEvaluation( d, fev, 0f);
                evals.add(newe);
            }
        }
        return evals;
    }

    public void selectSiteListener(AjaxBehaviorEvent event) throws PersistenceException {
        PersistenceManager pm = ServiceRegistry.getPersistenceManager();
        participants = pm.getParticipants(site);
        
        obsParticipants = new ArrayList<>();
        for (Participant p : participants) {
            if (p.getStudyArmStatus().equals(StudyArmStatus.OBSERVATIONAL_ARM)) {
                obsParticipants.add(p);
            }
        }
        
        participant = noParticipant;
        
        participants.add( participant);
        obsParticipants.add( participant);
        
        clearEvaluations();
        clearResults();
        clearChart();
    }
 
    public List<Participant> getParticipants() {
        
        return participants;
    }
    
    public List<Participant> getObsParticipants() {
        return obsParticipants;
    }
    
    public Participant getParticipant() {
        return participant;
    }
    
    public void setParticipant( Participant p) {
        participant = p;
    }
    
    public void selectParticipantListener(AjaxBehaviorEvent event) throws PersistenceException {
        
        if( "None".equals(participant.getParticipantID())) {
            clearEvaluations();
            clearResults();
            renderChart = false;
        }
        else {
            try {
                clearResults();
                renderChart = false;
                inputEvaluations = getEvaluationsForParticipant( participant);
            } catch (NullPointerException npe) {
                logger.error("Error in selectParticipantListener", npe);
            }
        }
    }
    
    private List<PulmonaryEvaluation> getEvaluationsForParticipant( Participant p) throws PersistenceException {
            
            clearEvaluations();
            
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            
            PersistenceManager pm = ServiceRegistry.getPersistenceManager();
            List<Event> events = pm.getEvents(p);
            
            Event event = null;
            for( Event e: events) {
                if( e.getType().equals( ECPEventTypes.ELIGIBILITY)) {
                    event = e;
                    break;
                }
            }
            
            EligibilityForm eligibilityForm = null;
            if( event != null) {
                List<BasicForm> forms = pm.getForms(event);
                for( BasicForm form: forms) {
                    if( form.getFormType().equals(ECPFormTypes.ELIGIBILITY)) {
                        eligibilityForm = pm.getEligibilityForm(form);
                        break;
                    }
                }
            }
            
            // Dynamically expand list of FEV1s available on calculator
            if( eligibilityForm != null) {
                List<PulmonaryEvaluation> evals = eligibilityForm.getEvaluations();
                if( evals.size() > inputEvaluations.size()) {
                    inputEvaluations = evals;
                }
                else {
                    for( int i = 0; i < evals.size(); i++) {
                       inputEvaluations.set(i, evals.get(i));
                    }
                }
            }
                    
        return inputEvaluations;
    }
    
    public void selectObservationParticipantListener(AjaxBehaviorEvent event) throws PersistenceException {
        
        if( "None".equals(participant.getParticipantID())) {
            clearEvaluations();
            clearResults();
            renderChart = false;
        }
        else {
            try {
                clearResults();
                renderChart = false;
                inputEvaluations = getObservationEvaluationsForParticipant( participant);
            } catch (NullPointerException npe) {
                logger.error("Error in selectParticipantListener", npe);
            }
        }
    }
    
    private List<PulmonaryEvaluation> getObservationEvaluationsForParticipant( Participant p) throws PersistenceException {
        clearEvaluations();

        List<PulmonaryEvaluation> allEvaluations = new ArrayList<>();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        PersistenceManager pm = ServiceRegistry.getPersistenceManager();
        List<Event> events = pm.getEvents(p);

        Event event = null;
        for( Event e: events) {
            if( e.getType().equals( ECPEventTypes.ELIGIBILITY)) {
                event = e;
                break;
            }
        }

        EligibilityForm eligibilityForm = null;
        if( event != null) {
            List<BasicForm> forms = pm.getForms(event);
            for( BasicForm form: forms) {
                if( form.getFormType().equals(ECPFormTypes.ELIGIBILITY)) {
                    eligibilityForm = pm.getEligibilityForm(form);
                    break;
                }
            }
        }

        // Dynamically expand list of FEV1s available on calculator
        if( eligibilityForm != null) {
            List<PulmonaryEvaluation> evals = eligibilityForm.getEvaluations();
            if( evals.size() > allEvaluations.size()) {
                allEvaluations = evals;
            }
            else {
                for( int i = 0; i < evals.size(); i++) {
                   allEvaluations.set(i, evals.get(i));
                }
            }
        }

        List<Event> eventsList = pm.getEvents(p);  // Get the list of events for the participant since there isn't currently any software written to get the form directly from the participant.
        List<PulmonaryEvaluation> additionalEvals = new ArrayList<>();
        for (Event e : eventsList) {
            if (e.getType() == ECPEventTypes.PULMONARY_EVAL && e.getStatus() == EventStatus.DCC_VERIFIED) {
                PulmEvalForm pft = pm.getPulmEvalForm(pm.getForms(e).get(0));

                additionalEvals.add(new PulmonaryEvaluation(pft.getDate(), pft.getFev1(), pft.getFvc()));
            }
        }
        
        allEvaluations.addAll(additionalEvals);

        inputEvaluations = allEvaluations;
                    
        return inputEvaluations;
    }
    
    public boolean isRenderChart() {
        return renderChart;
    }
    
    // Used for site dropdown to resolve site object from site ID
    public Converter getSiteConverter() {
        return siteConverter;
    }
    
    public class SiteConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext context, UIComponent component, String value) {
            Site site = defaultSite;
            for (Site s : sites) {
                if( s.getName().equals(value)) {
                    site = s;
                    break;
                }
            }
            return site ;
       }

        @Override
        public String getAsString(FacesContext context, UIComponent component, Object value) {
            Site site = (Site) value;
            return site.getName();
        }
    }
 
    // Used for participant dropdown to resolve participant object from participant
    public Converter getParticipantConverter() {
        return participantConverter;
    }
    
    public class ParticipantConverter implements Converter {
        @Override
        public Object getAsObject(FacesContext context, UIComponent component, String value) {
            Participant participant = noParticipant;
            for( Participant p : participants) {
                if( p.getParticipantID().equals(value)) {
                    participant = p;
                    break;
                }
            }
            return participant ;
       }

        @Override
        public String getAsString(FacesContext context, UIComponent component, Object value) {
            Participant participant = (Participant) value;
            return participant.getParticipantID();
        }
    }
 
}
