package edu.wustl.mir.ctt.calc;

import edu.wustl.mir.ctt.form.CrossoverEligibilityWorkSheet;
import edu.wustl.mir.ctt.model.Participant;
import edu.wustl.mir.ctt.model.PulmonaryEvaluation;
import edu.wustl.mir.ctt.model.Site;
import edu.wustl.mir.ctt.persistence.PersistenceException;
import edu.wustl.mir.ctt.persistence.PersistenceManager;
import edu.wustl.mir.ctt.persistence.ServiceRegistry;
import edu.wustl.mir.ctt.util.DateUtil;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import edu.wustl.mir.ctt.log.AuditLogger;

/**
 *
 * @author drm
 */
public class CrossoverEligibilityCalculator {
    private AuditLogger logger;
    
    private enum CrossoverStatus {
        INELIGIBLE_PARTICIPANT_ON_HOLD,         // no action 0
        INELIGIBLE_TOO_FEW_RECENT_FEV1S,        // no action 1
        INELIGIBLE_TOO_FEW_RECENT_FEV1S_LONG,   // no action 1
        ELIGIBLE_FEW_FEV1S_SLOPE_SIG_OK,        // To safety check 2
        INELIGIBLE_FEW_FEV1S_SLOPE_SIG_NOT_OK,  // no action 3
        ELIGIBLE_FOUR_FEV1S_SLOPE_SIG_OK,       // To safety check 4
        ELIGIBLE_FOUR_FEV1S_OVERRIDE,           // To clinical override 5
        INELIGIBLE_FOUR_FEV1S_SLOPE_SIG_NOT_OK, // Keep in observational arm after 4 FEV1s  6
        INELIGIBLE_MORE_THAN_FOUR_FEV1S,        // Too many FEV1s
        INELIGIBLE_LATEST_FEV1_TOO_OLD_7_1,          // no action 7
        INELIGIBLE_LATEST_FEV1_TOO_OLD
    }
    
    private static final Map<CrossoverStatus, String> rules;
    static {
        Map<CrossoverStatus, String> r = new EnumMap<>(CrossoverStatus.class);
        r.put(CrossoverStatus.INELIGIBLE_PARTICIPANT_ON_HOLD, "The participant is on two-month hold period.");
        r.put(CrossoverStatus.INELIGIBLE_TOO_FEW_RECENT_FEV1S, "Fewer than five FEV1 values in the last six months.");
        r.put(CrossoverStatus.INELIGIBLE_TOO_FEW_RECENT_FEV1S_LONG, "Fewer than five FEV1 values in the last nine months.");
        r.put(CrossoverStatus.ELIGIBLE_FEW_FEV1S_SLOPE_SIG_OK, "The slope meets criteria and is significant.");
        r.put(CrossoverStatus.INELIGIBLE_FEW_FEV1S_SLOPE_SIG_NOT_OK, "The participant does not meet the slope requirements or the slope is not statistically significant.");
        r.put(CrossoverStatus.ELIGIBLE_FOUR_FEV1S_SLOPE_SIG_OK, "the slope meets criteria and is significant.");
        r.put(CrossoverStatus.ELIGIBLE_FOUR_FEV1S_OVERRIDE, "the slope meets criteria but is not significant.");
        r.put(CrossoverStatus.INELIGIBLE_FOUR_FEV1S_SLOPE_SIG_NOT_OK, "the slope does not meet criteria.");
        r.put(CrossoverStatus.INELIGIBLE_LATEST_FEV1_TOO_OLD_7_1, "No FEV1 values in the last 7 days.");
        r.put(CrossoverStatus.INELIGIBLE_LATEST_FEV1_TOO_OLD, "No FEV1 values in the last 30 days.");
        rules = r;
    }
   
    private static final Map<CrossoverStatus, String> msgs;
    static {
        Map<CrossoverStatus, String> m = new EnumMap<>(CrossoverStatus.class);
        m.put(CrossoverStatus.INELIGIBLE_PARTICIPANT_ON_HOLD, "Participant ineligible for crossover assessment - This participant is still within the 2-month hold period and may not be re-assessed for crossover into the ECP Treatment Arm at this time.  This participant remains in the Observational Arm.  The earliest date on which this patient may be re-assessed for crossover is %s.  Please contact your CCC nurse coordinator if you have questions.");
        m.put(CrossoverStatus.INELIGIBLE_TOO_FEW_RECENT_FEV1S, "Patient ineligible for crossover assessment – Including the most recently entered pulmonary function tests, this patient does not have at least five FEV-1 values that were obtained at least one week apart within the last 6 months, including one FEV-1 value within the last 7 days.  Please obtain and enter subsequent pulmonary assessments before re-assessing for crossover.  Please contact your CCC nurse coordinator if you have questions.");
        m.put(CrossoverStatus.INELIGIBLE_TOO_FEW_RECENT_FEV1S_LONG, "Patient ineligible for crossover assessment – Including the most recently entered pulmonary function tests, this patient does not have at least five FEV-1 values that were obtained at least one week apart within the last 6 months, including one FEV-1 value within the last 30 days.  Please obtain and enter subsequent pulmonary assessments before re-assessing for crossover.  Please contact your CCC nurse coordinator if you have questions.");
        m.put(CrossoverStatus.ELIGIBLE_FEW_FEV1S_SLOPE_SIG_OK, "The participant is eligible to cross over into the ECP Treatment Arm.");
        m.put(CrossoverStatus.INELIGIBLE_FEW_FEV1S_SLOPE_SIG_NOT_OK, "This patient remains in Observation. See ECP Protocol v7.1 (2/28/18) Section 3.6.1. Please contact the Central Coordinating Center at WUSTL if you have questions.");
        m.put(CrossoverStatus.ELIGIBLE_FOUR_FEV1S_SLOPE_SIG_OK, "The participant is eligible to cross over into the ECP Treatment Arm.");
        m.put(CrossoverStatus.ELIGIBLE_FOUR_FEV1S_OVERRIDE, "The participant does not meet all of the criteria for inclusion in the ECP treatment arm. Do you wish to override this and move the participant anyway?");
        m.put(CrossoverStatus.INELIGIBLE_FOUR_FEV1S_SLOPE_SIG_NOT_OK, "The participant does not meet the slope criteria for inclusion in the ECP treatment arm. The Participant Remains in Observation.");
        m.put(CrossoverStatus.INELIGIBLE_LATEST_FEV1_TOO_OLD_7_1, "The crossover calculator can only be run within 7 days of a pulmonary function evaluation. The last evaluation was done on %s.");
        m.put(CrossoverStatus.INELIGIBLE_LATEST_FEV1_TOO_OLD, "The crossover calculator can only be run within 30 days of a pulmonary function evaluation. The last evaluation was done on %s.");
        msgs = m;
    }
    
    private StudyArmEligibilityCalculator eligCalc;
    private CrossoverStatus index;
    private Participant p;
    private boolean displayCalculatorResults = false;
    private List<PulmonaryEvaluation> evals;
    private Date screenDate;
    
    public CrossoverEligibilityCalculator( CrossoverEligibilityWorkSheet form, Site site) throws PersistenceException {
        this( form.getEvaluations(), form.getDate(), form.getParticipantID(), site);
    }
    
    public CrossoverEligibilityCalculator( List<PulmonaryEvaluation> evals, Date screenDate, String participantID, Site site) throws PersistenceException {
        this.logger = AuditLogger.create(CrossoverEligibilityCalculator.class);
        this.evals = evals;
        this.screenDate = screenDate;
        PersistenceManager pm = ServiceRegistry.getPersistenceManager();
        p = pm.getParticipant( participantID);
        index = screen(site);
    }
    
    public CrossoverEligibilityCalculator( List<PulmonaryEvaluation> evals, Date screenDate, Participant participant, Site site) {
        this.logger = AuditLogger.create(CrossoverEligibilityCalculator.class);
        this.evals = evals;
        this.screenDate = screenDate;
        this.p = participant;
        index = screen(site);
    }
    
    /*
     * Use this constructor in tests.  Set the participant without a call to DB
     * and then call 'screen()'.
    */
    public CrossoverEligibilityCalculator( List<PulmonaryEvaluation> evals, Date screenDate) {
        this.logger = AuditLogger.create(CrossoverEligibilityCalculator.class);
        this.evals = evals;
        this.screenDate = screenDate;
    }
    
    /*
     * handy for testing.
    */
    public void setParticipant( Participant p) {
        this.p = p;
    }
    
    public String getOutcomeMessage() throws PersistenceException {
        String msg;
        switch (index) {
            case INELIGIBLE_PARTICIPANT_ON_HOLD:
                Date d = getEndOfHoldDate();
                SimpleDateFormat ft = new SimpleDateFormat ("MM-dd-yyyy");
                msg = String.format(msgs.get(CrossoverStatus.INELIGIBLE_PARTICIPANT_ON_HOLD), ft.format(d));
                break;
            case INELIGIBLE_TOO_FEW_RECENT_FEV1S:
                msg = msgs.get(CrossoverStatus.INELIGIBLE_TOO_FEW_RECENT_FEV1S);
                break;
            case ELIGIBLE_FEW_FEV1S_SLOPE_SIG_OK:
                msg = msgs.get(CrossoverStatus.ELIGIBLE_FEW_FEV1S_SLOPE_SIG_OK);
                break;
            case INELIGIBLE_FEW_FEV1S_SLOPE_SIG_NOT_OK:
                ft = new SimpleDateFormat ("MM-dd-yyyy");
                msg = String.format(msgs.get(CrossoverStatus.INELIGIBLE_FEW_FEV1S_SLOPE_SIG_NOT_OK), ft.format( getStartCollectionOf4FEV1sDate()));
                break;
            case ELIGIBLE_FOUR_FEV1S_SLOPE_SIG_OK:
                msg = msgs.get(CrossoverStatus.ELIGIBLE_FOUR_FEV1S_SLOPE_SIG_OK);
                break;
            case ELIGIBLE_FOUR_FEV1S_OVERRIDE:
                msg = msgs.get(CrossoverStatus.ELIGIBLE_FOUR_FEV1S_OVERRIDE);
                break;
            case INELIGIBLE_FOUR_FEV1S_SLOPE_SIG_NOT_OK:
                msg = msgs.get(CrossoverStatus.INELIGIBLE_FOUR_FEV1S_SLOPE_SIG_NOT_OK);
                break;
            case INELIGIBLE_LATEST_FEV1_TOO_OLD:
                d = getDateOfLastEvaluation();
                ft = new SimpleDateFormat ("MM-dd-yyyy");
                msg = String.format(msgs.get(CrossoverStatus.INELIGIBLE_LATEST_FEV1_TOO_OLD), ft.format(d));
                break;
            default:
                msg = "Unknown outcome from CrossoverEligibilityCalculator: status = " + index;
                break;
        }
        return msg;
    }
    
    public boolean getDisplayCalculatorResults() {
        return displayCalculatorResults;
    }
    
    public String getOutcomeRule() {
        return rules.get(index);
    }

    public float getSlope() {
//        System.out.println("The slope is: " + eligCalc.getSlope() + "\n");
        return (float) eligCalc.getSlope();
    }
    
    public float getPValue() {
        return (float) eligCalc.getPValue();
    }
    
    public float getMinFev() {
        return eligCalc.getMinFev();
    }
    
    public Date getTimerStartDate() {
        if( p.getHoldStartDate() == null) {
            return DateUtils.addDays(p.getEnrolledDate(), 1); // do not count PFT on enrollment date
        }
        else {
            return p.getHoldStartDate();
        }
    }

    public static Date getTimerStartDate( Participant p) {
        if( p.getHoldStartDate() == null) {
            return DateUtils.addDays(p.getEnrolledDate(), 1); // do not count PFT on enrollment date
        }
        else {
            return p.getHoldStartDate();
        }
    }
   
    public Date getStartCollectionOf4FEV1sDate() {
        if( p.getHoldStartDate() == null) {
            return p.getEnrolledDate();
        }
        else {
            return getEndOfHoldDate();
        }
    }

    public Date getEndOfHoldDate() {
        Date d = getTimerStartDate();
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.add( Calendar.DAY_OF_MONTH, 60);
        return cal.getTime();
    }
    
    public static Date getEndOfHoldDate( Participant p) {
        Date d = getTimerStartDate(p);
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.add( Calendar.DAY_OF_MONTH, 60);
        return cal.getTime();
    }
    
    public Date getDateOfLastEvaluation() {
        Date d = null;
        if( evals.size() > 0) {
            d = evals.get(0).getDate();
            for( PulmonaryEvaluation e: evals) {
                if( e.getDate().after(d)) {
                    d = e.getDate();
                }
            }
        }
        return d;
    }
    
    public String getEligibilityString() {
        String outcome;
        
        if(null ==
                index) {
            outcome = "Calculator error";
        } else switch (index) {
            case ELIGIBLE_FEW_FEV1S_SLOPE_SIG_OK:
            case ELIGIBLE_FOUR_FEV1S_SLOPE_SIG_OK:
                outcome = "Participant is ELIGIBLE to cross over.";
                break;
            case ELIGIBLE_FOUR_FEV1S_OVERRIDE:
                outcome = "Participant is ELIGIBLE to cross over with override.";
                break;
            case INELIGIBLE_PARTICIPANT_ON_HOLD:
            case INELIGIBLE_TOO_FEW_RECENT_FEV1S:
            case INELIGIBLE_FEW_FEV1S_SLOPE_SIG_NOT_OK:
            case INELIGIBLE_FOUR_FEV1S_SLOPE_SIG_NOT_OK:
            case INELIGIBLE_LATEST_FEV1_TOO_OLD:
                outcome = "Participant is NOT ELIGIBLE to cross over.";
                break;
            default:
                outcome = "Calculator error";
                break;
        }
        
        return outcome;
    }
    
    public boolean isCrossoverEligible() {
        boolean b = false;
        if(index == CrossoverStatus.ELIGIBLE_FEW_FEV1S_SLOPE_SIG_OK ||
           index == CrossoverStatus.ELIGIBLE_FOUR_FEV1S_SLOPE_SIG_OK) {
            b = true;
        }
        return b;
    }
    
    /**
     * careful!  isNotCrossoverEligilble does not equal ( ! isCrossoverEligble)
     * due to existence of override eligible state.
     * 
     * @return 
     */
    public boolean isNotCrossoverEligible() {
        boolean b = false;
        if(index == CrossoverStatus.INELIGIBLE_PARTICIPANT_ON_HOLD ||
           index == CrossoverStatus.INELIGIBLE_TOO_FEW_RECENT_FEV1S ||
           index == CrossoverStatus.INELIGIBLE_FEW_FEV1S_SLOPE_SIG_NOT_OK ||
           index == CrossoverStatus.INELIGIBLE_LATEST_FEV1_TOO_OLD) {
            b = true;
        }
        return b;
    }
    
    public boolean isOverrideEligible() {
        boolean b = false;
        if( index == CrossoverStatus.ELIGIBLE_FOUR_FEV1S_OVERRIDE ) {
            b = true;
        }
        return b;
    }
    
    public boolean isKeepParticipantInObservationalArmEligible() {
        boolean b = false;
        if( index == CrossoverStatus.INELIGIBLE_FOUR_FEV1S_SLOPE_SIG_NOT_OK ){
            b = true;
        }
        return b;
    }
    
    private boolean isMeasurementWithinLastWeek( Date d, Date now) {
        double days = DateUtil.intervalInDays(d, now);
        return days < 8;
//        Date startDate = DateUtil.addDays(now, -7);
//        return DateUtil.isDateInIntervalInclusive( d, startDate, now);
    }
    
    private boolean isMeasurementWithinLastThirtyDays( Date d, Date now) {
        double days = DateUtil.intervalInDays(d, now);
        return days < 31;
//        Date startDate = DateUtil.addDays(now, -7);
//        return DateUtil.isDateInIntervalInclusive( d, startDate, now);
    }
    
    public StudyArmEligibilityCalculator getCalculator() {
        return eligCalc;
    }
    
    public List<PulmonaryEvaluation> getEvalsWithQualifyingDates() {
        return eligCalc.getEvalsWithQualifyingDates();
    }
    
    private CrossoverStatus screen(Site site) {
        String crfString = site.getCrfVersion();
        CrossoverStatus status;
        
        if (crfString.equals("7.1")) {
            status = screen_7_1(site);
        } else {
            status = screen_8_0(site);
        }
        
        return status;
    }
    
    private CrossoverStatus screen_7_1(Site site) {
        eligCalc = new StudyArmEligibilityCalculator( evals, screenDate, true, site);
        CrossoverStatus i;
                        
        if( p != null && p.isHoldStatus()) {
            i = CrossoverStatus.INELIGIBLE_PARTICIPANT_ON_HOLD;
            return i;
        } 

        // Calculate "data staleness" here instead of delegating to the Study Arm calculator
        // because the period in that calculator has changed to two weeks, not one
        if(  ! isMeasurementWithinLastWeek( getDateOfLastEvaluation(), screenDate) ) {
            i = CrossoverStatus.INELIGIBLE_LATEST_FEV1_TOO_OLD_7_1;
            return i;
        }

        /*
        if( eligCalc.isDataStale()) {
            i = CrossoverStatus.INELIGIBLE_LATEST_FEV1_TOO_OLD;
            return i;
        }
        */
        
        Date timerStartDate = getTimerStartDate();
        
        logger.audit("Timer start date is " + timerStartDate);
        logger.audit("Screen date is " + screenDate);
        List<PulmonaryEvaluation> spacedEvals = eligCalc.filterForMinimalSpacing(evals, 7);
       
        int monthsPrevious = 6;
        int additionalDaysPrevious = 0; //used for waiver
        int lastSixMonthsCount = eligCalc.getEvalsWithinLastMonths(spacedEvals, screenDate, monthsPrevious, additionalDaysPrevious).size();
        
        if(lastSixMonthsCount < 5) {
            i = CrossoverStatus.INELIGIBLE_TOO_FEW_RECENT_FEV1S;
            return i;
        }
        
        int fev1Count = getNumberOfMeasurementsInInterval( spacedEvals, timerStartDate, screenDate);
        logger.audit("Number of measurements since timer start at " + timerStartDate + " is " + fev1Count);
        
        // Calculate if will go on hold
        boolean canOverride;
        Date enrollDate = p.getEnrolledDate();
        List<PulmonaryEvaluation> evalsTowardHold = spacedEvals;
        
        // Filter out PFTs before enrollment
        List<PulmonaryEvaluation> evalsBeforeEnrollment = new ArrayList<>();
        for (PulmonaryEvaluation pe : evalsTowardHold) {
            if (pe.getDate().before(enrollDate) || pe.getDate().compareTo(enrollDate) == 0) {
                evalsBeforeEnrollment.add(pe);
            }
        }
        evalsTowardHold.removeAll(evalsBeforeEnrollment);
        
        while (evalsTowardHold.size() > 4) {
            List<PulmonaryEvaluation> previousHoldPeriod = new ArrayList<>();
            
            previousHoldPeriod.add(evalsTowardHold.get(0));
            previousHoldPeriod.add(evalsTowardHold.get(1));
            previousHoldPeriod.add(evalsTowardHold.get(2));
            previousHoldPeriod.add(evalsTowardHold.get(3));
            
            evalsTowardHold.removeAll(previousHoldPeriod);
            
            Date lastDateBeforeHold = previousHoldPeriod.get(3).getDate();
            Calendar cal = Calendar.getInstance();
            cal.setTime(lastDateBeforeHold);
            cal.add(Calendar.MONTH, 2);
            Date endOfHold = cal.getTime();
            
            List<PulmonaryEvaluation> evalsDuringHold = new ArrayList<>();
            for(PulmonaryEvaluation pe : evalsTowardHold) {
                if (pe.getDate().before(endOfHold)) {
                    evalsDuringHold.add(pe);
                    logger.audit("PFT at date " + pe.getDate() + " removed because occurred in hold period between " + lastDateBeforeHold + " and " + endOfHold);
                }
            }
            
            evalsTowardHold.removeAll(evalsDuringHold);
        }
        
        if(evalsTowardHold.size() < 4) {
            canOverride = false;
        } else {
            canOverride = true;
        }
        
        // Determine eligibility for crossover
        eligCalc = new StudyArmEligibilityCalculator( evals, screenDate, true, site); // Not necessary to send spacedEvals because StudyArmEligibilityCalculator calculates spacing
        displayCalculatorResults = true;
            
        if (canOverride == true) { // Allow override at each fourth FEV1
            if( eligCalc.isECPTreatmentArmEligible()) {
                i = CrossoverStatus.ELIGIBLE_FOUR_FEV1S_SLOPE_SIG_OK;
            }
            else if( eligCalc.isSlopeOKandStatsNotSig()){
                i = CrossoverStatus.ELIGIBLE_FOUR_FEV1S_OVERRIDE;
            } 
            else {  // Slope is not OK
                i = CrossoverStatus.INELIGIBLE_FOUR_FEV1S_SLOPE_SIG_NOT_OK;
            }
        
            return i;
        } else { 
            if( eligCalc.isSlopeOKandStatsSig()) {
                i = CrossoverStatus.ELIGIBLE_FEW_FEV1S_SLOPE_SIG_OK;
            }
            else {
                i = CrossoverStatus.INELIGIBLE_FEW_FEV1S_SLOPE_SIG_NOT_OK;
            }
        
            return i;
        }
    }
    
    private CrossoverStatus screen_8_0(Site site) {
        eligCalc = new StudyArmEligibilityCalculator( evals, screenDate, true, site);
        CrossoverStatus i;

        // Calculate "data staleness" here instead of delegating to the Study Arm calculator
        // because the period in that calculator is not the same
        if(  ! isMeasurementWithinLastThirtyDays( getDateOfLastEvaluation(), screenDate) ) {
            i = CrossoverStatus.INELIGIBLE_LATEST_FEV1_TOO_OLD;
            return i;
        }
        
        Date timerStartDate = getTimerStartDate();
        
        logger.audit("Timer start date is " + timerStartDate);
        logger.audit("Screen date is " + screenDate);
        List<PulmonaryEvaluation> spacedEvals = eligCalc.filterForMinimalSpacing(evals, 7);
        
        int monthsPrevious = 6;
        int additionalDaysPrevious = 0; //used for waiver
        int lastSixMonthsCount = eligCalc.getEvalsWithinLastMonths(spacedEvals, screenDate, monthsPrevious, additionalDaysPrevious).size();
        
        if(lastSixMonthsCount < 5) {
            i = CrossoverStatus.INELIGIBLE_TOO_FEW_RECENT_FEV1S;
            return i;
        }
        
        int fev1Count = getNumberOfMeasurementsInInterval( spacedEvals, timerStartDate, screenDate);
        logger.audit("Number of measurements since timer start at " + timerStartDate + " is " + fev1Count);
        
        // Determine the number of FEV1s within the time period.
        if(fev1Count < 4) {
            displayCalculatorResults = true;
            
            if( eligCalc.isSlopeOKandStatsSig()) {
                i = CrossoverStatus.ELIGIBLE_FEW_FEV1S_SLOPE_SIG_OK;
            }
            else {
                i = CrossoverStatus.INELIGIBLE_FEW_FEV1S_SLOPE_SIG_NOT_OK;
            }
        
            return i;
        } else { // 4 or more FEVs in last 4 weeks
            eligCalc = new StudyArmEligibilityCalculator( evals, screenDate, true, site);
            displayCalculatorResults = true;

            if( eligCalc.isECPTreatmentArmEligible()) {
                i = CrossoverStatus.ELIGIBLE_FOUR_FEV1S_SLOPE_SIG_OK;
            }
            else if( eligCalc.isSlopeOKandStatsNotSig()){
                i = CrossoverStatus.ELIGIBLE_FOUR_FEV1S_OVERRIDE;
            } 
            else {  // Slope is not OK
                i = CrossoverStatus.INELIGIBLE_FOUR_FEV1S_SLOPE_SIG_NOT_OK;
            }
        
            return i;
        }
    }
    
//    private int getNumberOfMeasurementsInInterval( List<PulmonaryEvaluation> evals, Date startDate, int periodInDays) {
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(startDate);
//        cal.add(Calendar.DAY_OF_MONTH, periodInDays);
//        Date stopDate = cal.getTime();
//        if( periodInDays >=0) {
//            return getNumberOfMeasurementsInInterval( evals, startDate, stopDate);
//        }
//        else {
//            return getNumberOfMeasurementsInInterval( evals, stopDate, startDate);
//        }
//        
//    }
    
    private int getNumberOfMeasurementsInInterval( List<PulmonaryEvaluation> evals, Date startDate, Date stopDate) {
        int count = 0;
        
        for( PulmonaryEvaluation eval: evals) {
            // "Start date" is never the exact enrollment date due to offset built into getTimerStartDate method
            if ((eval.getDate().equals(startDate) || eval.getDate().after(startDate)) &&
                (eval.getDate().equals(stopDate) || eval.getDate().before(stopDate))) {
                logger.audit("Found date " + eval.getDate() + " FEV1 " + eval.getFev1() + " in range");
                count++;
            }
        }
        return count;
    }
        
    public String getNextCrossoverDate() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DAY_OF_MONTH, 60);
        Date date = cal.getTime();
        SimpleDateFormat ft = new SimpleDateFormat ("MM-dd-yyyy");
        return ft.format(date);
    }
    
    // ToDo: this was kludged up in a hurry.  Refactor this when this calculator is integrated with original eligibility calculator.
    public static boolean isHoldExpired( Participant p, Date d) {
        Date hd = getEndOfHoldDate(p);
        return d.after(hd);
    }
    
}
