package edu.wustl.mir.ctt.calc;

import edu.wustl.mir.ctt.form.StudyArmEligibilityForm;
import edu.wustl.mir.ctt.model.PulmonaryEvaluation;
import edu.wustl.mir.ctt.model.Site;
import edu.wustl.mir.ctt.util.DateUtil;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import edu.wustl.mir.ctt.log.AuditLogger;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;

/**
 * Screen the participant for eligibility and study-arm assignment.
 * 
 * Uses all measurements provided. Only screens for minimum data requirements.
 *
 * @author drm
 */
public class StudyArmEligibilityCalculator implements Serializable {    
    private enum EligibilityStatus {
        INELIGIBLE_TOO_FEW_FEV1S,                 // fewer than five sufficiently spaced FEV1s
        INELIGIBLE_TOO_FEW_FEV1S_LONG,            // fewer than five sufficiently spaced FEV1s (longer window as of protocol 8.0
        INELIGIBLE_LATEST_FEV1_TOO_OLD,           // latest fev1 more than 14 days old
        TREATMENT_HIGH_FEV1_ELIGIBLE,             // minFev >= 1200 and slope < -30 and pvalue < 0.05
        OBSERVATION_HIGH_FEV1_INSIGNIFICANT,      // minFev >= 1200 and slope < -30 and pvalue >= 0.05
        OBSERVATION_HIGH_FEV1_DECLINE_NOT_ENOUGH, // minFev >= 1200 and slope >= -30 
        TREATMENT_LOW_FEV1_ELIGIBLE,              // minFev < 1200 and slope < -10 and pvalue < 0.05
        OBSERVATION_LOW_FEV1_INSIGNIFICANT,       // minFev < 1200 and slope < -10 and pvalue >= 0.05
        OBSERVATION_LOW_FEV1_DECLINE_NOT_ENOUGH,  // minFev < 1200 and slope >= -10
        INELIGIBLE_LATEST_FEV1_TOO_LOW,           // latest fev1 < 900
        INELIGIBLE_WBC_COUNT_TOO_LOW,             // wbc count below threshold for leukopenia (3000 cells/mm3)
        INELIGIBLE_LATEST_EXAM_TOO_OLD,           // latest physical exam is older than two weeks
        INELIGIBLE_LATEST_CBC_TOO_OLD,            // latest complete blood count is older than two weeks
        INELIGIBLE_MEASUREMENTS_NOT_REGULAR,      // fev1s in the past 4 months have interval greater than 8 weeks
        INELIGIBLE_DECLINE_TOO_HIGH,              // slope <= -300
        INELIGIBLE_DIAGNOSIS_CRITERIA_NOT_MET,    // diagnosis FEV1s are not 20% lower than ISHLT baseline
        INELIGIBLE_DIAGNOSIS_CRITERIA_NOT_MET_2,  // diagnosis FEV1s are not properly spaced
        INELIGIBLE_DIAGNOSIS_PFT_INCORRECT,       // first diagnosis FEV1 should be used as diagnosis date
        INELIGIBLE_MEASUREMENTS_NOT_REGULAR_LONG  // fev1s in the past 4 months have interval greater than 8 weeks
    }
    
    // Use status enumeration as key for message map for readability
    private static final Map<EligibilityStatus, String> rules;
    static { // Initialize static map, see https://stackoverflow.com/a/507658
        Map<EligibilityStatus, String> r = new EnumMap<>(EligibilityStatus.class);
        r.put(EligibilityStatus.INELIGIBLE_TOO_FEW_FEV1S, "In the previous 6 months, there are not at least 5 measurements that are all spaced at least 14 days apart.");
        r.put(EligibilityStatus.INELIGIBLE_TOO_FEW_FEV1S_LONG, "In the previous 9 months, there are not at least 5 measurements that are all spaced at least 14 days apart.");
        r.put(EligibilityStatus.INELIGIBLE_LATEST_FEV1_TOO_OLD, "The most recent measurement must be within two weeks prior to enrollment.");
        r.put(EligibilityStatus.TREATMENT_HIGH_FEV1_ELIGIBLE, "meets the criteria when the minimum FEV1 >= 1200 ml (slope < -30 AND p-value < 0.05).");
        r.put(EligibilityStatus.OBSERVATION_HIGH_FEV1_INSIGNIFICANT, "the p-value >= 0.05 when the minimum FEV1 >= 1200 ml and the slope < -30.");
        r.put(EligibilityStatus.OBSERVATION_HIGH_FEV1_DECLINE_NOT_ENOUGH, "because the slope >= -30 when the minimum FEV1 >= 1200 ml.");
        r.put(EligibilityStatus.TREATMENT_LOW_FEV1_ELIGIBLE, "meets the criteria when the minimum FEV1 < 1200 ml (slope < -10 AND p-value < 0.05).");
        r.put(EligibilityStatus.OBSERVATION_LOW_FEV1_INSIGNIFICANT, "the p-value >= 0.05 when the minimum FEV1 < 1200 ml and the slope < -10.");
        r.put(EligibilityStatus.OBSERVATION_LOW_FEV1_DECLINE_NOT_ENOUGH, "because the slope >= -10 when the minimum FEV1 < 1200 ml.");
        r.put(EligibilityStatus.INELIGIBLE_LATEST_FEV1_TOO_LOW, "The most recent FEV1 is too low.");
        r.put(EligibilityStatus.INELIGIBLE_WBC_COUNT_TOO_LOW, "The white blood cell count is less than 3 K/cumm.");
        r.put(EligibilityStatus.INELIGIBLE_LATEST_EXAM_TOO_OLD, "The documented physical assessment must be within two weeks prior to enrollment.");
        r.put(EligibilityStatus.INELIGIBLE_LATEST_CBC_TOO_OLD, "The documented complete blood count must be within two weeks prior to enrollment.");
        r.put(EligibilityStatus.INELIGIBLE_MEASUREMENTS_NOT_REGULAR, "The measurements obtained in the last four months were not regular (interval between measurements exceeded eight weeks).");
        r.put(EligibilityStatus.INELIGIBLE_DECLINE_TOO_HIGH, "The slope is <= -300.");
        r.put(EligibilityStatus.INELIGIBLE_DIAGNOSIS_CRITERIA_NOT_MET, "The laboratory based FEV1 values used to diagnose BOS were not at least 20% lower than baseline FEV1.");
        r.put(EligibilityStatus.INELIGIBLE_DIAGNOSIS_CRITERIA_NOT_MET_2, "The laboratory based FEV1 values used to diagnose BOS were not at least three weeks apart.");
        r.put(EligibilityStatus.INELIGIBLE_DIAGNOSIS_PFT_INCORRECT, "DATA ENTRY ERROR NOTED: Laboratory-based FEV1 values used to confirm the initial diagnosis of NEW BOS is defined as the date at which the first FEV1 value recorded fell below the threshold. Please click the Review Data Entry button below and confirm data entered in Section D.");
        r.put(EligibilityStatus.INELIGIBLE_MEASUREMENTS_NOT_REGULAR_LONG, "The measurements obtained in the last six months were not regular (interval between measurements exceeded twelve weeks).");
        rules = Collections.unmodifiableMap(r);
    }
   
    private static final Map<EligibilityStatus, String> msgs;
    static { // Initialize static map, see https://stackoverflow.com/a/507658
        Map<EligibilityStatus, String> m = new EnumMap<>(EligibilityStatus.class);
        m.put(EligibilityStatus.INELIGIBLE_TOO_FEW_FEV1S, "Patient not eligible. " + rules.get(EligibilityStatus.INELIGIBLE_TOO_FEW_FEV1S));
        m.put(EligibilityStatus.INELIGIBLE_TOO_FEW_FEV1S_LONG, "Patient not eligible. " + rules.get(EligibilityStatus.INELIGIBLE_TOO_FEW_FEV1S_LONG));
        m.put(EligibilityStatus.INELIGIBLE_LATEST_FEV1_TOO_OLD, "Patient not eligible. " + rules.get(EligibilityStatus.INELIGIBLE_LATEST_FEV1_TOO_OLD));
        m.put(EligibilityStatus.TREATMENT_HIGH_FEV1_ELIGIBLE, "ECP Treatment Arm eligible, " + rules.get(EligibilityStatus.TREATMENT_HIGH_FEV1_ELIGIBLE));
        m.put(EligibilityStatus.OBSERVATION_HIGH_FEV1_INSIGNIFICANT, "Observation Arm eligible, " + rules.get(EligibilityStatus.OBSERVATION_HIGH_FEV1_INSIGNIFICANT));
        m.put(EligibilityStatus.OBSERVATION_HIGH_FEV1_DECLINE_NOT_ENOUGH, "Observation Arm eligible " + rules.get(EligibilityStatus.OBSERVATION_HIGH_FEV1_DECLINE_NOT_ENOUGH));
        m.put(EligibilityStatus.TREATMENT_LOW_FEV1_ELIGIBLE, "ECP Treatment Arm eligible, " + rules.get(EligibilityStatus.TREATMENT_LOW_FEV1_ELIGIBLE));
        m.put(EligibilityStatus.OBSERVATION_LOW_FEV1_INSIGNIFICANT, "Observation Arm eligible, " + rules.get(EligibilityStatus.OBSERVATION_LOW_FEV1_INSIGNIFICANT));
        m.put(EligibilityStatus.OBSERVATION_LOW_FEV1_DECLINE_NOT_ENOUGH, "Observation Arm eligible " + rules.get(EligibilityStatus.OBSERVATION_LOW_FEV1_DECLINE_NOT_ENOUGH));
        m.put(EligibilityStatus.INELIGIBLE_LATEST_FEV1_TOO_LOW, "Patient not eligible. " + rules.get(EligibilityStatus.INELIGIBLE_LATEST_FEV1_TOO_LOW));
        m.put(EligibilityStatus.INELIGIBLE_WBC_COUNT_TOO_LOW, "Patient not eligible. " + rules.get(EligibilityStatus.INELIGIBLE_WBC_COUNT_TOO_LOW));
        m.put(EligibilityStatus.INELIGIBLE_LATEST_EXAM_TOO_OLD, "Patient not eligible. " + rules.get(EligibilityStatus.INELIGIBLE_LATEST_EXAM_TOO_OLD));
        m.put(EligibilityStatus.INELIGIBLE_LATEST_CBC_TOO_OLD, "Patient not eligible. " + rules.get(EligibilityStatus.INELIGIBLE_LATEST_CBC_TOO_OLD));
        m.put(EligibilityStatus.INELIGIBLE_DIAGNOSIS_CRITERIA_NOT_MET, "Patient not eligible. " + rules.get(EligibilityStatus.INELIGIBLE_DIAGNOSIS_CRITERIA_NOT_MET));
        m.put(EligibilityStatus.INELIGIBLE_DIAGNOSIS_CRITERIA_NOT_MET_2, "Patient not eligible. " + rules.get(EligibilityStatus.INELIGIBLE_DIAGNOSIS_CRITERIA_NOT_MET_2));
        m.put(EligibilityStatus.INELIGIBLE_DIAGNOSIS_PFT_INCORRECT, "Data entry issue. " + rules.get(EligibilityStatus.INELIGIBLE_DIAGNOSIS_PFT_INCORRECT));
        m.put(EligibilityStatus.INELIGIBLE_DECLINE_TOO_HIGH, "Patient not eligible. " + rules.get(EligibilityStatus.INELIGIBLE_DECLINE_TOO_HIGH));
        m.put(EligibilityStatus.INELIGIBLE_MEASUREMENTS_NOT_REGULAR, "Patient not eligible. " + rules.get(EligibilityStatus.INELIGIBLE_MEASUREMENTS_NOT_REGULAR));
        m.put(EligibilityStatus.INELIGIBLE_MEASUREMENTS_NOT_REGULAR_LONG, "Patient not eligible. " + rules.get(EligibilityStatus.INELIGIBLE_MEASUREMENTS_NOT_REGULAR_LONG));
        msgs = Collections.unmodifiableMap(m);
    }

    private SlopeEstimator estimator;
    private EligibilityStatus status;
    private float minFev;
    private float lastFev;
    private float slope;
    private float pvalue;
    private List<PulmonaryEvaluation> evals;
    private List<PulmonaryEvaluation> evalsWithQualifyingDates;
    private Date screenDate;
    private String fev1OldestPossibleDate;
    private Date lastExamDate;
    private Date lastCbcDate;
    private Float wbcs;
    private Float baseline;
    private List<Float> diagFEVValues;
    private Date earliestDiagFEVDate;
    private Date latestDiagFEVDate;
    private Date diagDate;
    
    private AuditLogger logger;
    
    public StudyArmEligibilityCalculator( StudyArmEligibilityForm form, Site site) {
        logger = AuditLogger.create(StudyArmEligibilityCalculator.class);
        
        this.evals = form.getPulmEvaluations(); // Get last year's worth of pulmonary evaluations
        this.screenDate = form.getDate();
        this.lastExamDate = form.getMostRecentExamDate();
        this.lastCbcDate = form.getCompleteBloodCountDate();
        this.wbcs = form.getWbcs();
        
        this.baseline = form.getBaselineFEV1();
        
        diagFEVValues = new ArrayList<>();
        this.diagFEVValues.add(form.getFirstPostTransBOSDiagFEV1());
        this.diagFEVValues.add(form.getSecondPostTransBOSDiagFEV1());
        if(form.getThirdPostTransBOSDiagFEV1() != null) {
            this.diagFEVValues.add(form.getThirdPostTransBOSDiagFEV1());
        }
        
        diagDate = form.getPostTransBOSDiagDate();
        logger.audit("Setting nBOS diagnosis date to " + form.getPostTransBOSDiagDate());
        
        earliestDiagFEVDate = form.getFirstPostTransBOSDiagFEV1Date();
        logger.audit("Setting earliest diagnosis FEV1 date to " + form.getFirstPostTransBOSDiagFEV1Date());
        if (form.getSecondPostTransBOSDiagFEV1Date().before(earliestDiagFEVDate)) {
            logger.audit("Setting earliest diagnosis FEV1 date to " + form.getSecondPostTransBOSDiagFEV1Date());
            earliestDiagFEVDate = form.getSecondPostTransBOSDiagFEV1Date();
        }
        if (form.getThirdPostTransBOSDiagFEV1Date() != null && form.getThirdPostTransBOSDiagFEV1Date().before(earliestDiagFEVDate)) {
            logger.audit("Setting earliest diagnosis FEV1 date to " + form.getThirdPostTransBOSDiagFEV1Date());
            earliestDiagFEVDate = form.getThirdPostTransBOSDiagFEV1Date();
        }
        
        this.latestDiagFEVDate = form.getFirstPostTransBOSDiagFEV1Date();
        if (form.getSecondPostTransBOSDiagFEV1Date().after(latestDiagFEVDate)) {
            logger.audit("Setting latest diagnosis FEV1 date to " + form.getSecondPostTransBOSDiagFEV1Date());
            this.latestDiagFEVDate = form.getSecondPostTransBOSDiagFEV1Date();
        }
        if (form.getThirdPostTransBOSDiagFEV1Date() != null && form.getThirdPostTransBOSDiagFEV1Date().after(latestDiagFEVDate)) {
            logger.audit("Setting latest diagnosis FEV1 date to " + form.getThirdPostTransBOSDiagFEV1Date());
            this.latestDiagFEVDate = form.getThirdPostTransBOSDiagFEV1Date();
        }
        
        status = screen(true, false, site);
    }
    
    public StudyArmEligibilityCalculator( List<PulmonaryEvaluation> evals, Date screenDates, boolean crossover, Site site) {
        logger = AuditLogger.create(StudyArmEligibilityCalculator.class);
        
        this.evals = evals;
        this.screenDate = screenDates;
       
        this.lastExamDate = null;
        this.lastCbcDate = null;
        this.wbcs = null;
        this.baseline = null;
        this.diagDate = null;
        this.diagFEVValues = null;
        
        logger.audit("In the StudyArmEligibilityCalculator, crossover was " + crossover + " in constructor");
        
//        logger.audit("The screenDate is the form.getDate: " + screenDate);
        // do not do inclusion/exclusion criteria checks
        status = screen(false, crossover, site);
    }

    public List<PulmonaryEvaluation> getEvaluations() {
        return evals;
    }

    public List<PulmonaryEvaluation> getEvalsWithQualifyingDates() {
        return evalsWithQualifyingDates;
    }

    public Date getScreenDate() {
        return screenDate;
    }

    public void setScreenDate(Date screenDate) {
        this.screenDate = screenDate;
    }
    
    public String getOutcomeMessage() {
        return msgs.get(status);
    }
    
    public String getOutcomeRule() {
        return rules.get(status);
    }
    
    public float getPValue() {
        return pvalue;
    }
    
    public float getSlope() {
        return slope;
    }
    
    public double predict( Date d) {
        // If you get an error message whne trying to run the estimator.predict(d) command, it most likely is due to the esitmator not being
        // instantiated in the initialScreen() method below (see the line of code 'estimator = new SlopeEstimator( dates, fevs)' in the initialScreen() method).
        return estimator.predict(d);
    }
    
    public float getMinFev() {
        return minFev;
    }
    
    public float getLastFev() {
        return lastFev;
    }
    
    public String getFev1OldestPossibleDate() {
        return fev1OldestPossibleDate;
    }
    
    public String getStudyEligibilityOutcome() {
        String outcome = "";
        
        if (isStudyEligible()) {
            outcome = "Participant is ELIGIBLE to be enrolled into the study.";
        } else {
            outcome = "Participant is NOT ELIGIBLE to be enrolled into the study. ";
        }
        
        return outcome;
    }
    
    public boolean isStudyEligible() {
        return isECPTreatmentArmEligible() || isObservationalArmEligible();
    }
    
    public boolean isECPTreatmentArmEligible() {
        boolean b = false;
        
        if(status == EligibilityStatus.TREATMENT_HIGH_FEV1_ELIGIBLE ||
           status == EligibilityStatus.TREATMENT_LOW_FEV1_ELIGIBLE) {
            b = true;
        }
        
        return b;
    }
    
    public boolean isObservationalArmEligible() {
        boolean b = false;
        
        if(status == EligibilityStatus.OBSERVATION_HIGH_FEV1_INSIGNIFICANT ||
           status == EligibilityStatus.OBSERVATION_HIGH_FEV1_DECLINE_NOT_ENOUGH ||
           status == EligibilityStatus.OBSERVATION_LOW_FEV1_INSIGNIFICANT  ||
           status == EligibilityStatus.OBSERVATION_LOW_FEV1_DECLINE_NOT_ENOUGH) {
            b = true;
        }
        
        return b;
    }
    
    public boolean isSlopeOKandStatsSig() {
        return (status == EligibilityStatus.TREATMENT_HIGH_FEV1_ELIGIBLE) ||
               (status == EligibilityStatus.TREATMENT_LOW_FEV1_ELIGIBLE);
    }
    
    public boolean isSlopeOKandStatsNotSig() {
        return (status == EligibilityStatus.OBSERVATION_HIGH_FEV1_INSIGNIFICANT) ||
               (status == EligibilityStatus.OBSERVATION_LOW_FEV1_INSIGNIFICANT);
    }
    
    public boolean isDataStale() {
        return status == EligibilityStatus.INELIGIBLE_LATEST_FEV1_TOO_OLD;
    }
    
    public boolean isDataTooFew() {
        return status == EligibilityStatus.INELIGIBLE_TOO_FEW_FEV1S;
    }
    
    public boolean isLastMeasurementTooLow() {
        return status == EligibilityStatus.INELIGIBLE_LATEST_FEV1_TOO_LOW;
    }
    
    public int numberOfMinimallySpacedMeasurementsInPeriod( List<PulmonaryEvaluation> evals, Date startDate, Date stopDate, float spacingInDays) {
        evalsWithQualifyingDates = filterForMinimalSpacing( evals, spacingInDays);
        
//        int j;
//        for(int i=0; i < qualifyingDates.size(); i++){
//            j = i + 1;
//            logger.audit("The qualifying dates are: " + j + "   " + qualifyingDates.get(i).getDate().toString());
//        }
        return countInInterval( evalsWithQualifyingDates, startDate, stopDate);
    }
    
    public List<PulmonaryEvaluation> filterForMinimalSpacing( List<PulmonaryEvaluation> evals, float spacingInDays) {
        evalsWithQualifyingDates = new ArrayList<>();
        
        if( evals.size() > 1) {
            evalsWithQualifyingDates.add( evals.get(0));
            Date d1, d2;
            for( int i = 0; i < evals.size(); i++) {
                d1 = evals.get(i).getDate();
                for( int j = i+1; j < evals.size(); j++) {
                    d2 = evals.get(j).getDate();
                    // The minus 0.1 needed to be taken away from the spacingInDays because of the change
                    // in Central Standard Time (CST) to Daylight Savings Time (DST).  The day of the time  
                    // change, the number of hours in a day are 23 not 24 so a day is less than 1, but we 
                    // expect the days in the interval to all have 24 hours.  The minus 0.1 takes care of 
                    // the change in time in the spring.
                    if( DateUtil.intervalInDays( d1, d2) >= spacingInDays - 0.1) {
                        evalsWithQualifyingDates.add( evals.get(j));
                        i = j - 1;
                        break;
                    } else {
                        logger.audit("Spacing was inadequate for data point at " + d2);
                        logger.audit("Interval was " + DateUtil.intervalInDays(d1, d2));
                        logger.audit("Required spacing was " + (spacingInDays - 0.1));
                    }
                }
            }
        }
        
        return evalsWithQualifyingDates;
    }
    
    public int countInInterval( List<PulmonaryEvaluation> evals, Date startDate, Date stopDate) {
        int cnt = 0;
        double days;
        days = DateUtil.intervalInDays(startDate, stopDate);
        for( PulmonaryEvaluation e: evals) {
            if( DateUtil.isDateInIntervalInclusive(e.getDate(), startDate, stopDate)) {
                logger.audit("Date " + e.getDate() + " was in interval between " + startDate + " and " + stopDate);
                cnt++;
            } else {
                logger.audit("Date " + e.getDate() + " was not in interval between " + startDate + " and " + stopDate);
            }
        }
        logger.audit("The count in the interval between " + startDate + " and " + stopDate + " is: " + cnt);
        return cnt;
    }
    
    public boolean wereMeasurementsObtainedRegularly(List<PulmonaryEvaluation> evals, int thresholdMonths, int intervalDays) {
        boolean measurementsAtCorrectInterval = true;
        List<PulmonaryEvaluation> filteredEvals = new ArrayList<>();
        
        // Sort the array of PulmonaryEvaluation objects by date in ascending order.
        Collections.sort(evals, new Comparator<PulmonaryEvaluation>() {
            @Override
            public int compare(PulmonaryEvaluation pe1, PulmonaryEvaluation pe2) {
                if (pe1.getDate() == null || pe2.getDate() == null)
                    return 0;
                return pe1.getDate().compareTo(pe2.getDate());
            }
        });
        
        // Remove any dates older than six months from the list.
        Date monthThreshold = DateUtil.addMonths(screenDate, -thresholdMonths);
        for (PulmonaryEvaluation p : evals) {
            if(p.getDate().equals(monthThreshold) || p.getDate().after(monthThreshold)){
                filteredEvals.add(p);
            }
        }
        
        for (int i = 0; (i + 1) < filteredEvals.size(); i++) {
            Date currentDate = filteredEvals.get(i).getDate();
            Date spacingMonthThreshold = DateUtil.addDays(currentDate, intervalDays);
            Date nextDate = filteredEvals.get(i + 1).getDate();
            
            logger.audit("Current date " + currentDate + " and next date " + nextDate);
            logger.audit("Spacing threshold " + spacingMonthThreshold + " (interval " + intervalDays + ")");
            
            // is the next measurement further away than the threshold? (greater than required spacing?)
            if (nextDate.after(spacingMonthThreshold)) {
                measurementsAtCorrectInterval = false;
                logger.audit("Spacing failure");
            } else {
                logger.audit("Spacing passed");
            }
        }
        
        return measurementsAtCorrectInterval;
    }
    
    private boolean isMeasurementWithinLastTwoWeeks( Date d, Date now) {
        double days = DateUtil.intervalInDays(d, now);
        return days < 15;
//        Date startDate = DateUtil.addDays(now, -7);
//        return DateUtil.isDateInIntervalInclusive( d, startDate, now);
    }
    
    private boolean areDiagnosisEvaluationsSpaced( Date firstDiag, Date secondDiag) {
        Date threshold = DateUtil.addDays(firstDiag, 21);
        
        return (secondDiag.compareTo(threshold) >= 0); // -1 compare result means that the second diagnosis PFT took place before 21 days, too soon 
    }
      
    private Date getDateOfLastEvaluation() {
        Date d = null;
        if( evals.size() > 0) {
            d = evals.get(0).getDate();
            for( PulmonaryEvaluation e: evals) {
                if( e.getDate().after(d)) {
                    d = e.getDate();
                }
            }
        }
        return d;
    }
    
    public List<PulmonaryEvaluation> getEvalsWithinLastMonths(List<PulmonaryEvaluation> pulmEvals, Date screenDate, int numMonths, int numDays){
//        List<PulmonaryEvaluation> pulmEvals = new ArrayList<PulmonaryEvaluation>();
        List<PulmonaryEvaluation> pulmEvals2 = new ArrayList<>();
        
        // Sort the array of PulmonaryEvaluation objects by date in ascending order.
        Collections.sort(pulmEvals, new Comparator<PulmonaryEvaluation>() {
            @Override
            public int compare(PulmonaryEvaluation pe1, PulmonaryEvaluation pe2) {
                if (pe1.getDate() == null || pe2.getDate() == null)
                    return 0;
                return pe1.getDate().compareTo(pe2.getDate());
            }
        });

        // Remove any dates older than six months from the list.
        PulmonaryEvaluation pe;
        Date threshold = DateUtil.addMonths(screenDate, -numMonths);
        threshold = DateUtil.addDays(threshold, -numDays);
        logger.audit("getEvalsWithinLastMonths threshold is " + threshold);
        
        Iterator iterator = pulmEvals.iterator();
        while(iterator.hasNext()){
            pe = (PulmonaryEvaluation) iterator.next();
            
            if (!pe.getDate().before(threshold)) {
                pulmEvals2.add(pe);
                logger.audit("Included eval within threshold at " + pe.getDate());
            } else {
                logger.audit("Excluded eval within threshold at " + pe.getDate());
            }
        }

        return pulmEvals2;

    }
    
    private EligibilityStatus screen(boolean enrollmentScreen, boolean crossoverScreen, Site site) {
        String crfString = site.getCrfVersion();
        EligibilityStatus status;
        
        if (crfString.equals("7.1")) {
            status = screen_7_1(enrollmentScreen, crossoverScreen);
        } else {
            status = screen_8_0(enrollmentScreen, crossoverScreen);
        }
        
        return status;
    }
    
    private EligibilityStatus screen_7_1(boolean enrollmentScreen, boolean crossoverScreen) {
        List<Date> dates = new ArrayList<>();
        List<Float> fevs = new ArrayList<>();
        EligibilityStatus i;
        
        logger.audit("In StudyArmEligibilityCalculator.screen, enrollmentscreen is " + enrollmentScreen + " and crossoverscreen is " + crossoverScreen);
        
        if (enrollmentScreen) {
            logger.audit("Checking enrollment screening...");
            if (wbcs != null && wbcs < 3.0) {
                i = EligibilityStatus.INELIGIBLE_WBC_COUNT_TOO_LOW;
                return i;
            }

            if (lastExamDate != null && DateUtil.intervalInDays(lastExamDate, screenDate) > 15) {
                i = EligibilityStatus.INELIGIBLE_LATEST_EXAM_TOO_OLD;
                return i;
            }

            if (lastCbcDate != null && DateUtil.intervalInDays(lastCbcDate, screenDate) > 15) {
                i = EligibilityStatus.INELIGIBLE_LATEST_CBC_TOO_OLD;
                return i;
            }

            // Check that BOS diagnosis values are 20% less than baseline
            if (baseline != null && diagFEVValues != null) {
                Float threshold = baseline * 0.805f;

                for (Float diagFEV : diagFEVValues) {
                    if (diagFEV > threshold) {
                        i = EligibilityStatus.INELIGIBLE_DIAGNOSIS_CRITERIA_NOT_MET;
                        return i;
                    }
                }
            }
            
            // Check that diagnosis PFTs are appropriately spaced
            if (earliestDiagFEVDate != null && latestDiagFEVDate != null) {
                if (!areDiagnosisEvaluationsSpaced(earliestDiagFEVDate, latestDiagFEVDate)) {
                    i = EligibilityStatus.INELIGIBLE_DIAGNOSIS_CRITERIA_NOT_MET_2;
                    return i;
                }
            }
        }
        
        Collections.sort(evals, new Comparator<PulmonaryEvaluation>() {
            @Override
            public int compare(PulmonaryEvaluation pe1, PulmonaryEvaluation pe2) {
                if (pe1.getDate() == null || pe2.getDate() == null)
                    return 0;
                return pe1.getDate().compareTo(pe2.getDate());
            }
        });
        
        int nValues = evals.size();
        lastFev = evals.get(nValues - 1).getFev1() * 1000f;
        
        if (!crossoverScreen) {
            logger.audit("Crossover screen was false");
            if (lastFev <= 900) {
                logger.audit("Last FEV was " + lastFev + " and lower than 900");
                i = EligibilityStatus.INELIGIBLE_LATEST_FEV1_TOO_LOW;
                return i;
            } else {
                logger.audit("Last FEV was " + lastFev + " and not lower than 900");
            }
        }
        
        // Format the start of the six month period so the oldest possible FEV1 date can be determined.
        // The fev1OldestPossibleDate is used enrollStudyArmElig.xhtml form in the Omnifaces o:validateMultiple
        // validators to let the individual filling out the form know the oldest possible FEV1 date that can be
        // entered into the form.
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        Date fev1SixMonthsPrevious = DateUtil.addMonths(screenDate, -6);
        Date fev1SixMonthsThreshold = DateUtil.addDays(fev1SixMonthsPrevious, -1);
        
        String fev1OldestDate = sdf.format(fev1SixMonthsThreshold);
        
        // If a oldest date is six months ago, use the date below to show the person entering the dates on the Elig Form.
        // We do not want to show them a date of exactly six months as above.
        fev1OldestPossibleDate = sdf.format(fev1SixMonthsPrevious);

        // Use exactly six months for calculation
        int monthsPrevious = 6;
        int additionalDaysPrevious = 0; //28; //used for waiver
        
        if (crossoverScreen) {
            monthsPrevious = 6;
            additionalDaysPrevious = 0; // used for waiver
        }
        
        Date startDate = DateUtil.addMonths(screenDate, -monthsPrevious);
        startDate = DateUtil.addDays(startDate, -additionalDaysPrevious); // Use this for waiver
        logger.audit("The startDate is the screenDate - 6 months: " + startDate);
        logger.audit("The stopDate is the screenDate: " + screenDate);

        // Get the FEV1 Dates and values within the last six months from the initialScreen date.
        evalsWithQualifyingDates = this.getEvalsWithinLastMonths(evals, screenDate, monthsPrevious, additionalDaysPrevious);
        
        // SPECIAL NOTE: The number fourteen (14) used in the call to numberOfMinimallySpacedMeasurementsInPeriod refers to the
        // number of days in the spacing between measurement periods.
        // NOTE: The change from Central Standard Time to Central Daylight Time in the spring causes one day to be only 23 hours long, so
        // a full period does not occur between two dates if the spring time date change is between the dates.
        // If you follow the method numberOfMinimallySpacedMeasurementsInPeriod you will find we subtracted 0.1 days from the period to account
        // for the change in time.
        int screenInterval;
        
        if (crossoverScreen) {
            screenInterval = 7;
        } else {
            screenInterval = 14;
        }
        
        int countMeasurementsSufficientlySpaced = numberOfMinimallySpacedMeasurementsInPeriod( evalsWithQualifyingDates, startDate, screenDate, screenInterval);
        logger.audit("The countMeasurementsSufficientlySpaced is: " + countMeasurementsSufficientlySpaced + "\n");
                
        int minimumNumEvals;
        if (crossoverScreen) {
            minimumNumEvals = 5;
        } else {
            minimumNumEvals = 5;
        }
        
        if( countMeasurementsSufficientlySpaced < minimumNumEvals) {
            i = EligibilityStatus.INELIGIBLE_TOO_FEW_FEV1S;
            return i;
        }
        
        if (!crossoverScreen) {
            // Check to make sure that there are measurements 
            if (!wereMeasurementsObtainedRegularly(evalsWithQualifyingDates, 4, 57)) {
                i = EligibilityStatus.INELIGIBLE_MEASUREMENTS_NOT_REGULAR;
                return i;
            }
        }
        
        // Was the last FEV1 (Pulmonary Evaluation) performed within the last two weeks?
        if(!crossoverScreen && ! isMeasurementWithinLastTwoWeeks( getDateOfLastEvaluation(), screenDate) ) {
            i = EligibilityStatus.INELIGIBLE_LATEST_FEV1_TOO_OLD;
            return i;
        }

        for( PulmonaryEvaluation eval: evalsWithQualifyingDates) {
            dates.add(eval.getDate());
            fevs.add(eval.getFev1() * 1000f);
        }
           
        int indexOfMinValue = fevs.indexOf( Collections.min(fevs));
        minFev = fevs.get(indexOfMinValue);
        
        logger.audit("LAST FEV is " + lastFev);
        logger.audit("MINIMUM FEV is " + minFev);
        
        estimator = new SlopeEstimator( dates, fevs);
        
        slope = (float) estimator.getSlope();
        pvalue = (float) estimator.getPValue();
        
        if (minFev >= 1200) {
            if( slope < -30) {
                if ( slope > -300) {
                    if( pvalue < 0.05) {
                        i = EligibilityStatus.TREATMENT_HIGH_FEV1_ELIGIBLE;    // minFev >= 1200 and slope < -30 and pvalue < 0.05
                    }
                    else { // pvalue >= 0.05
                        i = EligibilityStatus.OBSERVATION_HIGH_FEV1_INSIGNIFICANT;    // minFev >= 1200 and slope < -30 and pvalue >= 0.05
                    }
                }
                else {
                    i = EligibilityStatus.INELIGIBLE_DECLINE_TOO_HIGH; // slope <= -300
                }
            }
            else { // slope >= -30
                i = EligibilityStatus.OBSERVATION_HIGH_FEV1_DECLINE_NOT_ENOUGH;    // minFev >= 1200 and slope >= -30
            }

        } else { // minFev < 1200

            if( slope < -10) {
                if ( slope > -300) {
                    if( pvalue < 0.05) {
                        i = EligibilityStatus.TREATMENT_LOW_FEV1_ELIGIBLE;    // minFev < 1200 and slope < -10 and pvalue < 0.05
                    }
                    else { // pvalue >= 0.05
                        i = EligibilityStatus.OBSERVATION_LOW_FEV1_INSIGNIFICANT;    // minFev < 1200 and slope < -10 and pvalue >= 0.05
                    }
                }
                else {
                    i = EligibilityStatus.INELIGIBLE_DECLINE_TOO_HIGH; // slope <= -300
                }
            }
            else { // slope >= -10
                i = EligibilityStatus.OBSERVATION_LOW_FEV1_DECLINE_NOT_ENOUGH;    // minFev < 1200 and slope >= -10 
            }

        }
            
        return i;
    }
    
    private EligibilityStatus screen_8_0(boolean enrollmentScreen, boolean crossoverScreen) {
        List<Date> dates = new ArrayList<>();
        List<Float> fevs = new ArrayList<>();
        EligibilityStatus i;
        
        logger.audit("In StudyArmEligibilityCalculator.screen, enrollmentscreen is " + enrollmentScreen + " and crossoverscreen is " + crossoverScreen);
        
        Collections.sort(evals, new Comparator<PulmonaryEvaluation>() {
            @Override
            public int compare(PulmonaryEvaluation pe1, PulmonaryEvaluation pe2) {
                if (pe1.getDate() == null || pe2.getDate() == null)
                    return 0;
                return pe1.getDate().compareTo(pe2.getDate());
            }
        });
        
        int nValues = evals.size();
        lastFev = evals.get(nValues - 1).getFev1() * 1000f;
        
        if (enrollmentScreen) {
            logger.audit("Checking enrollment screening...");
            if (wbcs != null && wbcs < 3.0) {
                i = EligibilityStatus.INELIGIBLE_WBC_COUNT_TOO_LOW;
                return i;
            }

            if (lastExamDate != null && DateUtil.intervalInDays(lastExamDate, screenDate) > 15) {
                i = EligibilityStatus.INELIGIBLE_LATEST_EXAM_TOO_OLD;
                return i;
            }

            if (lastCbcDate != null && DateUtil.intervalInDays(lastCbcDate, screenDate) > 15) {
                i = EligibilityStatus.INELIGIBLE_LATEST_CBC_TOO_OLD;
                return i;
            }

            // Check that BOS diagnosis values are 20% less than baseline
            if (baseline != null && diagFEVValues != null) {
                Float threshold = baseline * 0.805f;

                for (Float diagFEV : diagFEVValues) {
                    if (diagFEV > threshold) {
                        i = EligibilityStatus.INELIGIBLE_DIAGNOSIS_CRITERIA_NOT_MET;
                        return i;
                    }
                }
            }
            
            // Check that diagnosis PFTs are properly spaced
            if (earliestDiagFEVDate != null && diagDate != null) {
                if (!earliestDiagFEVDate.equals(diagDate)) {
                    i = EligibilityStatus.INELIGIBLE_DIAGNOSIS_PFT_INCORRECT;
                    return i;
                }
            }
            
            float minFevThreshold;
            
            if (baseline >= 3.0) {
                minFevThreshold = 900;
            } else {
                minFevThreshold = (baseline * 1000f) * 0.3f;
            }
            
            if (lastFev <= minFevThreshold) {
                logger.audit("Last FEV was " + lastFev + " and lower than " + minFevThreshold);
                i = EligibilityStatus.INELIGIBLE_LATEST_FEV1_TOO_LOW;
                return i;
            } else {
                logger.audit("Last FEV was " + lastFev + " and not lower than " + minFevThreshold);
            }
        }
      
        // SPECIAL NOTE: The number fourteen (14) used in the call to numberOfMinimallySpacedMeasurementsInPeriod refers to the
        // number of days in the spacing between measurement periods.
        // NOTE: The change from Central Standard Time to Central Daylight Time in the spring causes one day to be only 23 hours long, so
        // a full period does not occur between two dates if the spring time date change is between the dates.
        // If you follow the method numberOfMinimallySpacedMeasurementsInPeriod you will find we subtracted 0.1 days from the period to account
        // for the change in time.
        int screenInterval;
        int monthsPrevious;
        
        if (crossoverScreen) {
            screenInterval = 7;
            monthsPrevious = 6;
        } else {
            screenInterval = 14;
            monthsPrevious = 9;
        }
        
        // Format the start of the nine month period so the oldest possible FEV1 date can be determined.
        // The fev1OldestPossibleDate is used enrollStudyArmElig.xhtml form in the Omnifaces o:validateMultiple
        // validators to let the individual filling out the form know the oldest possible FEV1 date that can be
        // entered into the form.        
        int additionalDaysPrevious = 0; //used for waiver
        int monthsSpaced = 4;
        int spacing = 57;
        boolean passedRegularMeasurements = true;
        boolean passedEnoughEvals = true;
        
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        Date fev1MonthsPrevious = DateUtil.addMonths(screenDate, -monthsPrevious);
        Date fev1MonthsThreshold = DateUtil.addDays(fev1MonthsPrevious, -1);
        
        String fev1OldestDate = sdf.format(fev1MonthsThreshold);
        
        // If a oldest date is nine months ago, use the date below to show the person entering the dates on the Elig Form.
        // We do not want to show them a date of exactly nine months as above.
        fev1OldestPossibleDate = sdf.format(fev1MonthsPrevious);

        // Use exactly nine months for calculation        
        Date startDate = DateUtil.addMonths(screenDate, -monthsPrevious);
        logger.audit("The startDate is the screenDate - " + monthsPrevious + " months: " + startDate);
        logger.audit("The stopDate is the screenDate: " + screenDate);

        // Get the FEV1 Dates and values within the last nine months from the initialScreen date.
        evalsWithQualifyingDates = this.getEvalsWithinLastMonths(evals, screenDate, monthsPrevious, additionalDaysPrevious);
        
        int countMeasurementsSufficientlySpaced = numberOfMinimallySpacedMeasurementsInPeriod( evalsWithQualifyingDates, startDate, screenDate, screenInterval);
        logger.audit("The countMeasurementsSufficientlySpaced is: " + countMeasurementsSufficientlySpaced + "\n");
                
        int minimumNumEvals;
        if (crossoverScreen) {
            minimumNumEvals = 5;
        } else {
            minimumNumEvals = 5;
        }
        
        if( countMeasurementsSufficientlySpaced < minimumNumEvals) {
            passedEnoughEvals = false;
        } else {
            passedEnoughEvals = true;
        }
        
        if (!crossoverScreen && passedEnoughEvals) {
            // Check to make sure that there are measurements 
            if (wereMeasurementsObtainedRegularly(evalsWithQualifyingDates, monthsSpaced, spacing)) {
                passedRegularMeasurements = true;
            } else {
                passedRegularMeasurements = false;
            }
        }
        
        // Fall back to nine months if needed
        if ((passedEnoughEvals == false || passedRegularMeasurements == false) && crossoverScreen == false) {
            monthsPrevious = 9;
            monthsSpaced = 6;
            spacing = 85;

            fev1MonthsPrevious = DateUtil.addMonths(screenDate, -monthsPrevious);
            fev1MonthsThreshold = DateUtil.addDays(fev1MonthsPrevious, -1);

            fev1OldestDate = sdf.format(fev1MonthsThreshold);

            // If a oldest date is nine months ago, use the date below to show the person entering the dates on the Elig Form.
            // We do not want to show them a date of exactly nine months as above.
            fev1OldestPossibleDate = sdf.format(fev1MonthsPrevious);

            // Use exactly nine months for calculation
            startDate = DateUtil.addMonths(screenDate, -monthsPrevious);
            logger.audit("The startDate is the screenDate - " + monthsPrevious + " months: " + startDate);
            logger.audit("The stopDate is the screenDate: " + screenDate);

            // Get the FEV1 Dates and values within the last nine months from the initialScreen date.
            evalsWithQualifyingDates = this.getEvalsWithinLastMonths(evals, screenDate, monthsPrevious, additionalDaysPrevious);

            countMeasurementsSufficientlySpaced = numberOfMinimallySpacedMeasurementsInPeriod( evalsWithQualifyingDates, startDate, screenDate, screenInterval);
            logger.audit("The countMeasurementsSufficientlySpaced is: " + countMeasurementsSufficientlySpaced + "\n");

            if( countMeasurementsSufficientlySpaced < minimumNumEvals) {
                passedEnoughEvals = false;
            } else {
                passedEnoughEvals = true;
            }
        }
        
        if (!crossoverScreen && passedEnoughEvals) {
            // Check to make sure that there are measurements 
            if (wereMeasurementsObtainedRegularly(evalsWithQualifyingDates, monthsSpaced, spacing)) {
                passedRegularMeasurements = true;
            } else {
                passedRegularMeasurements = false;
            }
        }
        
        if (false == passedEnoughEvals) {
            i = EligibilityStatus.INELIGIBLE_TOO_FEW_FEV1S_LONG;
            return i;
        } else if (false == passedRegularMeasurements) {
            i = EligibilityStatus.INELIGIBLE_MEASUREMENTS_NOT_REGULAR_LONG;
            return i;
        }
        
        // Was the last FEV1 (Pulmonary Evaluation) performed within the last two weeks?
        if(!crossoverScreen && ! isMeasurementWithinLastTwoWeeks( getDateOfLastEvaluation(), screenDate) ) {
            i = EligibilityStatus.INELIGIBLE_LATEST_FEV1_TOO_OLD;
            return i;
        }

        for( PulmonaryEvaluation eval: evalsWithQualifyingDates) {
            dates.add(eval.getDate());
            fevs.add(eval.getFev1() * 1000f);
        }
           
        int indexOfMinValue = fevs.indexOf( Collections.min(fevs));
        minFev = fevs.get(indexOfMinValue);
        
        logger.audit("LAST FEV is " + lastFev);
        logger.audit("MINIMUM FEV is " + minFev);
        
        estimator = new SlopeEstimator( dates, fevs);
        
        slope = (float) estimator.getSlope();
        pvalue = (float) estimator.getPValue();
        
        if (minFev >= 1200) {
            if( slope < -30) {
                if ( slope > -300) {
                    if( pvalue < 0.05) {
                        i = EligibilityStatus.TREATMENT_HIGH_FEV1_ELIGIBLE;    // minFev >= 1200 and slope < -30 and pvalue < 0.05
                    }
                    else { // pvalue >= 0.05
                        i = EligibilityStatus.OBSERVATION_HIGH_FEV1_INSIGNIFICANT;    // minFev >= 1200 and slope < -30 and pvalue >= 0.05
                    }
                }
                else {
                    i = EligibilityStatus.INELIGIBLE_DECLINE_TOO_HIGH; // slope <= -300
                }
            }
            else { // slope >= -30
                i = EligibilityStatus.OBSERVATION_HIGH_FEV1_DECLINE_NOT_ENOUGH;    // minFev >= 1200 and slope >= -30
            }

        } else { // minFev < 1200

            if( slope < -10) {
                if ( slope > -300) {
                    if( pvalue < 0.05) {
                        i = EligibilityStatus.TREATMENT_LOW_FEV1_ELIGIBLE;    // minFev < 1200 and slope < -10 and pvalue < 0.05
                    }
                    else { // pvalue >= 0.05
                        i = EligibilityStatus.OBSERVATION_LOW_FEV1_INSIGNIFICANT;    // minFev < 1200 and slope < -10 and pvalue >= 0.05
                    }
                }
                else {
                    i = EligibilityStatus.INELIGIBLE_DECLINE_TOO_HIGH; // slope <= -300
                }
            }
            else { // slope >= -10
                i = EligibilityStatus.OBSERVATION_LOW_FEV1_DECLINE_NOT_ENOUGH;    // minFev < 1200 and slope >= -10 
            }

        }
            
        return i;
    }
}
