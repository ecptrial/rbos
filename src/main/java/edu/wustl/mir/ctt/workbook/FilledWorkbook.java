package edu.wustl.mir.ctt.workbook;

import edu.wustl.mir.ctt.ReportsController;
import edu.wustl.mir.ctt.reports.ReportItem;
import edu.wustl.mir.ctt.reports.ReportItems;
import edu.wustl.mir.ctt.log.AuditLogger;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.*;

public class FilledWorkbook {
    private Workbook wb;
    private ArrayList<String> cols;
    private int colsLen;
    private Sheet sheet;
     private AuditLogger logger;
    private boolean published;
    private String preset;
    private FormulaEvaluator fe;

    public FilledWorkbook(String preset) {
        logger = AuditLogger.create(ReportsController.class);
        this.wb = new XSSFWorkbook();
        this.cols = new ArrayList<>();
        this.colsLen = 0;
        this.preset = preset;
        this.published = false;
        this.fe = wb.getCreationHelper().createFormulaEvaluator();
        switch(preset){
            case "george":
                this.sheet = wb.createSheet("George Report");
                break;
            case "monthlyAccrual":
                this.sheet = wb.createSheet("Monthly Accrual Report");
                break;
            case "totalAccrual":
                this.sheet = wb.createSheet("Total Accrual Report");
                break;
            case "monthlySiteCRFStatus":
                this.sheet = wb.createSheet("Monthly Site CRF Status Report");
                break;

            default:
                logger.audit("No preset found!");
        }
    }
    public void generateMonthlySiteCRFStatus(LinkedList<Object[]> rows) {
        if (!this.published && preset.equals("monthlySiteCRFStatus")) {
            colsLen = 7;
            Row header = this.sheet.createRow(0);
            header.createCell(0).setCellValue("Site Name");
            header.createCell(1).setCellValue("Participant ID");
            header.createCell(2).setCellValue("Enrolled Date");
            header.createCell(3).setCellValue("Event Name");
            header.createCell(4).setCellValue("Event Status");
            header.createCell(5).setCellValue("Projected Date");
            header.createCell(6).setCellValue("Days From Event");

            int rowIter = 1;
            ReportsController rc = new ReportsController();
            for (Object[] row: rows){
                header = this.sheet.createRow(rowIter++);

                header.createCell(0).setCellValue(Objects.toString(row[0]));
                header.createCell(1).setCellValue(Integer.parseInt(Objects.toString(row[1])));
                header.createCell(2).setCellValue(Objects.toString(rc.convertDateToValidString((Date) row[2], "MM/dd/yyyy")));
                header.createCell(3).setCellValue(Objects.toString(row[3]));
                header.createCell(4).setCellValue(Objects.toString(row[4]));
                header.createCell(5).setCellValue(Objects.toString(rc.convertDateToValidString((Date) row[5],"MM/dd/yyyy" )));
                header.createCell(6).setCellValue(Integer.parseInt(Objects.toString(row[6])));

            }


        } else {
            logger.audit("Workbook already closed and published. Or this is the wrong method for your preset");

        }
    }
    public void generateTotalAccrual(List<ReportItem> totalAccruals, List<ReportItem> totalAccrualsBySite){
        if (!this.published && preset.equals("totalAccrual")) {
            colsLen = 6;

            Row header = this.sheet.createRow(0);
            header.createCell(0).setCellValue("Report Item");
            header.createCell(1).setCellValue("Accrual Count");

            String reportItemName;
            for (ReportItem reportItem: totalAccruals){
                reportItemName = reportItem.getReportItemName();
                switch (reportItemName){
                    case "Treatment Arm": header = this.sheet.createRow(1);
                        header.createCell(0).setCellValue(reportItemName);
                        header.createCell(1).setCellValue(reportItem.getReportItemCount());
                        break;
                    case "Observational Arm": header = this.sheet.createRow(2);
                        header.createCell(0).setCellValue(reportItemName);
                        header.createCell(1).setCellValue(reportItem.getReportItemCount());
                        break;
                    case "Total": header = this.sheet.createRow(3);
                        header.createCell(0).setCellValue(reportItemName);
                        header.createCell(1).setCellValue(reportItem.getReportItemCount());
                        break;
                    case "CrossedOver": header = this.sheet.createRow(4);
                        header.createCell(0).setCellValue(reportItemName);
                        header.createCell(1).setCellValue(reportItem.getReportItemCount());
                        break;
                }

            }

            header = this.sheet.createRow(6);

            header.createCell(0).setCellValue("Site");
            header.createCell(1).setCellValue("Treatment Arm");
            header.createCell(2).setCellValue("Observational Arm");
            header.createCell(3).setCellValue("Total");
            header.createCell(4).setCellValue("CrossedOver");
            int rowIndex = 7;
            for (ReportItem reportItem: totalAccrualsBySite){
                header = this.sheet.createRow(rowIndex);
                header.createCell(0).setCellValue(reportItem.getReportItemSiteName());
                header.createCell(1).setCellValue(reportItem.getReportItemTreatmentArmCount());
                header.createCell(2).setCellValue(reportItem.getReportItemObservationalArmCount());
                header.createCell(3).setCellValue(reportItem.getReportItemTotalCount());
                header.createCell(4).setCellValue(reportItem.getReportItemCrossedOverCount());

                rowIndex++;
            }




        } else{
            logger.audit("Workbook already closed and published. Or this is the wrong method for your preset");
        }
    }
    public void generateMonthlyAccrual(Map<String,ArrayList<Map<String, Object>>> mam){
        if (!this.published && preset.equals("monthlyAccrual")){
            Font boldFont = wb.createFont();
            boldFont.setBold(true);



            String[] colsAccrual = { "Site Number", "Site Name", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", "Total" };
            colsLen = colsAccrual.length;
            int rowIndex = 0;
            for (String year: mam.keySet()){
                Row header = this.sheet.createRow(rowIndex);
                Cell c = header.createCell(0);
                c.setCellValue(year);
                c.getCellStyle().setFont(boldFont);


                header = this.sheet.createRow(++rowIndex);

                // Set columns for each yearly table
                for (int i = 0; i < colsLen; i++){
                    c = header.createCell(i);
                    c.setCellValue(colsAccrual[i]);
                    c.getCellStyle().setFont(boldFont);
                }

                header = this.sheet.createRow(++rowIndex);

                for (Map<String,Object> site: mam.get(year)){

                    for (String siteDesc: site.keySet()){
                        switch(siteDesc){
                            case "Site Number": header.createCell(0).setCellValue(Integer.parseInt(String.valueOf(site.get(siteDesc))));
                                break;
                            case "Site Name": header.createCell(1).setCellValue((String) site.get(siteDesc));
                                break;
                            case "Jan": header.createCell(2).setCellValue(Integer.parseInt(String.valueOf(site.get(siteDesc))));
                                break;
                            case "Feb": header.createCell(3).setCellValue(Integer.parseInt(String.valueOf(site.get(siteDesc))));
                                break;
                            case "Mar": header.createCell(4).setCellValue(Integer.parseInt(String.valueOf(site.get(siteDesc))));
                                break;
                            case "Apr": header.createCell(5).setCellValue(Integer.parseInt(String.valueOf(site.get(siteDesc))));
                                break;
                            case "May": header.createCell(6).setCellValue(Integer.parseInt(String.valueOf(site.get(siteDesc))));
                                break;
                            case "Jun": header.createCell(7).setCellValue(Integer.parseInt(String.valueOf(site.get(siteDesc))));
                                break;
                            case "Jul": header.createCell(8).setCellValue(Integer.parseInt(String.valueOf(site.get(siteDesc))));
                                break;
                            case "Aug": header.createCell(9).setCellValue(Integer.parseInt(String.valueOf(site.get(siteDesc))));
                                break;
                            case "Sep": header.createCell(10).setCellValue(Integer.parseInt(String.valueOf(site.get(siteDesc))));
                                break;
                            case "Oct": header.createCell(11).setCellValue(Integer.parseInt(String.valueOf(site.get(siteDesc))));
                                break;
                            case "Nov": header.createCell(12).setCellValue(Integer.parseInt(String.valueOf(site.get(siteDesc))));
                                break;
                            case "Dec": header.createCell(13).setCellValue(Integer.parseInt(String.valueOf(site.get(siteDesc))));
                                break;
                        }
                    }
                    Cell cell = header.createCell(14);
                    cell.setCellFormula("SUM(C"+(rowIndex+1)+ ":N" +(rowIndex+1)+ ")");
                    fe.evaluate(cell);
                    header = this.sheet.createRow(++rowIndex);



                }
                header.createCell(1).setCellValue("Total");
                int offset = mam.get(year).size(); //Number of sites

                for (int i = 2; i<= 13; i++){
                    Cell monthlyTotal = header.createCell(i);
                    CellReference cr = new CellReference(monthlyTotal);

                    String colRef = cr.convertNumToColString(i);
                    int row = cr.getRow() + 1;
                    String formula = "SUM("+colRef +""+ (row-offset) +":"+colRef +""+ (row-1) +")";

                    monthlyTotal.setCellFormula(formula);
                    fe.evaluate(monthlyTotal);
                }
                Cell yearlyTotal = header.createCell(14);

                CellReference cr = new CellReference(yearlyTotal);
                int row = cr.getRow() + 1;
                yearlyTotal.setCellFormula("SUM(C"+row+":N"+row+")");
                fe.evaluate(yearlyTotal);

                rowIndex+=2;
            }
        }
        else {
            logger.audit("Workbook already closed and published. Or this is the wrong method for your preset");

        }
    }

    public void generateGeorge(HashMap<String, Integer> hm, String year){
        if(!this.published && preset.equals("george"))
        {
                this.colsLen = hm.size();

                Row header = this.sheet.createRow(0);
                header.createCell(0).setCellValue("Enrollment Projections");

                header = this.sheet.createRow(1);

                header.createCell(0).setCellValue("Site Name");

                for (int i = 0; i < hm.size(); i++) {
                    header.createCell(i + 1).setCellValue((String)(hm.keySet().toArray()[i]));
                }

                header = this.sheet.createRow(2);

                header.createCell(0).setCellValue("Enrollment " + year);

                for (int i = 0; i < hm.size(); i++) {
                    header.createCell(i + 1).setCellValue((hm.get(hm.keySet().toArray()[i])));
                }


        }
        else{
            logger.audit("Workbook already closed and published. Or this is the wrong method for your preset");
        }
    }

    public void publishFile(String path){
        if(!this.published)
        {
            try {
                for (int i = 0; i < colsLen; i++){
                    sheet.autoSizeColumn(i);
                }

                FileOutputStream fos = new FileOutputStream(path);
                wb.write(fos);
                fos.flush();
                fos.close();
                wb.close();
                this.published = true;
            } catch (FileNotFoundException e) {
                logger.error("FileNotFoundException", e);
            } catch (IOException e) {
                logger.error("IOException", e);
            }
        }
        else{
            logger.audit("Workbook already closed and published.");
        }
    }
}

