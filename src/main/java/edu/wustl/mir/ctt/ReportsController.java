package edu.wustl.mir.ctt;

import edu.wustl.mir.ctt.model.Site;
import edu.wustl.mir.ctt.persistence.*;
import edu.wustl.mir.ctt.reports.ReportItem;
import edu.wustl.mir.ctt.reports.ReportItems;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.annotation.PostConstruct;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;

import edu.wustl.mir.ctt.workbook.FilledWorkbook;
import edu.wustl.mir.ctt.workbook.ResultSetWorkBook;
import edu.wustl.mir.ctt.log.AuditLogger;

@ManagedBean
@SessionScoped
public class ReportsController implements Serializable {

    private ReportItems reportItemsForEcpTreatmentArm;
    private ReportItems reportItemsForObservationArm;
    private ReportItems reportItemsForCurrentCRFStatus;
    // The dataModel cannot be serialized and saved to a hard drive because a NotSerializableException error occurs, 
    // so the dataModel is saved in the ReportItems.java code. The ReportItems.java code is not serialized.
    private transient DataModel dataModelEcpTreatmentArm;  // The transient causes the dataModelEcpTreatment to not be stored so you do not get a NotSerializableException error.
                                                        // But you need to do lazy loading for the getDataModelEcpTreatment() method below.
    private transient DataModel dataModelObservationArm;  // The transient causes the dataModelEcpObservation to not be stored so you do not get a NotSerializableException error.
                                                        // But you need to do lazy loading for the getDataModelEcpObservation() method below.
    private transient DataModel dataModelCurrentCRFStatus;
    
    private ArrayList<String> columnNames;

    private Date inputDate; //The cutoff date for enrollment. Today's date by default, but clients can change this through an input.

     private AuditLogger logger;
    private String crfOutcome;

    private String projectionOutcome;

    private String monthlyAccrualOutcome;

    private List<SelectItem> availableYears;

    private Date crfInputDate;

    private String projectionInputYear;
    // Constructor
    public ReportsController() {
        logger = AuditLogger.create(ReportsController.class);
        inputDate = getCurrentDate();
        crfInputDate = getCurrentDate();
        crfOutcome = "";
        projectionInputYear = getYear(getCurrentDate());
    }

    @PostConstruct
    public void init(){
    availableYears = new ArrayList<>();

    int currentYear = Integer.valueOf(getYear(getCurrentDate()));
    int startingYear = 2014;

    while (startingYear <= currentYear){
        availableYears.add(new SelectItem(startingYear, String.valueOf(startingYear)));

        startingYear++;
    }



    }

    public String getMonthlyAccrualOutcome() {
        return monthlyAccrualOutcome;
    }

    public void setMonthlyAccrualOutcome(String monthlyAccrualOutcome) {
        this.monthlyAccrualOutcome = monthlyAccrualOutcome;
    }

    public String getCrfOutcome() {
        return crfOutcome;
    }

    public void setCrfOutcome(String crfOutcome) {
        this.crfOutcome = crfOutcome;
    }

    public String getProjectionOutcome() {
        return projectionOutcome;
    }

    public void setProjectionOutcome(String projectionOutcome) {
        this.projectionOutcome = projectionOutcome;
    }

    public List<SelectItem> getAvailableYears() {
        return availableYears;
    }

    public void setAvailableYears(List<SelectItem> availableYears) {
        this.availableYears = availableYears;
    }

    public Date getCrfInputDate() {
        return crfInputDate;
    }

    public void setCrfInputDate(Date crfInputDate) {
        this.crfInputDate = crfInputDate;
    }

    public String getProjectionInputYear() {
        return projectionInputYear;
    }

    public void setProjectionInputYear(String projectionInputYear) {
        this.projectionInputYear = projectionInputYear;
    }
    public Date getInputDate() { return inputDate; }

    public void setInputDate(Date inputDate) { this.inputDate = inputDate; }

    public ReportItems getReportItemsForEcpTreatmentArm(){
        return reportItemsForEcpTreatmentArm;
    }
    
    public ReportItems getReportItemsForObservationArm(){
        return reportItemsForObservationArm;
    }
    
    public ReportItems getReportItemsForCurrentCRFStatus(){
        return reportItemsForCurrentCRFStatus;
    }
    
    public String keyContacts() {
        return "/keyContacts.xhtml?faces-redirect=true";
    }
    
    public String supportStaff() {
        return "/supportStaff.xhtml?faces-redirect=true";
    }
/*    
    public void viewEvent( ActionEvent ae) {
        String eventId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("selectedEventId");
        System.out.println( eventId);
    }
*/    
    public String editSiteAction() {
        return "/editSite.xhtml?faces-redirect=true";
    }
    
    public String clearFormAction() {
        return null;
    }
    
      
    public String homePage(){
        System.out.println("The home page button was selected!!");
        return "main.xhtml?faces-redirect=true";
    }
    
    public Date getCurrentDate(){
        Date currentDate = new Date();
        return currentDate;
    }
    
    // The getDataModelEcpTreatmentArm() method is used by the monthlyAccrualReport.xhtml code to dynamically display varying numbers of columns in the table.
    public DataModel getDataModelEcpTreatmentArm() throws PersistenceException {
        // The following three lines of code are needed for lazy loading if the dataModelEcpTreatment is transient.
        // Otherwise, if the dataModel is not transient, comment out these three lines.
        if(this.dataModelEcpTreatmentArm == null){
//            this.dataModel = this.getAccrualBySiteByMonthReportItems();  // This line of code works also, but it runs the getAccrualBySiteByMonthReportItems() again which is not necessary.
            this.dataModelEcpTreatmentArm = this.getReportItemsForEcpTreatmentArm().getDataModel();
        }
            return dataModelEcpTreatmentArm;
    }
// The getDataModelCurrentCRFStatus method for the CRF Status Report
    public DataModel getDataModelCurrentCRFStatus() throws PersistenceException {
        if(this.dataModelCurrentCRFStatus == null){
            this.dataModelCurrentCRFStatus = this.getReportItemsForCurrentCRFStatus().getDataModel();
        }
            return dataModelCurrentCRFStatus;
    }
    
    public void setDataModelEcpTreatmentArm(DataModel dataModelEcpTreatmentArm){
        this.dataModelEcpTreatmentArm = dataModelEcpTreatmentArm;
    }

    // The getDataModelObservationArm() method is used by the monthlyAccrualReport.xhtml code to dynamically display varying numbers of columns in the table.
    public DataModel getDataModelObservationArm() throws PersistenceException {
        // The following three lines of code are needed for lazy loading if the dataModelEcpObservation is transient.
        // Otherwise, if the dataModel is not transient, comment out these three lines.
        if(this.dataModelObservationArm == null){
//            this.dataModel = this.getAccrualBySiteByMonthReportItems();  // This line of code works also, but it runs the getAccrualBySiteByMonthReportItems() again which is not necessary.
            this.dataModelObservationArm = this.getReportItemsForObservationArm().getDataModel();
        }
            return dataModelObservationArm;
    }

    public void setDataModelEcpObservation(DataModel dataModelEcpObservation){
        this.dataModelObservationArm = dataModelEcpObservation;
    }

    public ArrayList<String> getColumnNames() {
            return columnNames;
    }

    public void setColumnNames(ArrayList<String> columnNames) {
            this.columnNames = columnNames;
    }


    /**
     * Indicate if the ECP Report Menu items can be rendered if you are a technical coordinator.
     * 
     * True if you are logged in as a technical coordinator.
     * 
     * @return 
     */
    public boolean renderECPReportMenuItems() {
        return SecurityManager.canVerifyForms();
    }


    public String EnrollmentProjectionReportAction() throws PersistenceException {
        this.createAccrualBySiteByMonthReportItems();
        return "/enrollmentProjectionReport.xhtml?faces-redirect=true";
    }
    // Return the newly created totalAccuralReport.xhtml to the browswer for display.

    public String TherakosReportAction() throws PersistenceException {

        return "/therakosReport.xhtml?faces-redirect=true";
    }

    public String MonthlySiteCRFStatusReportAction() throws PersistenceException {
        this.createAccrualBySiteByMonthReportItems();
        return "/monthlySiteCRFStatusReport.xhtml?faces-redirect=true";
    }

    // Return the newly created totalAccuralReport.xhtml to the browswer for display. 
    // NOTE: the data within the totalAccrualReport.xhtml is created by each method call within the xhtml code.
    public String TotalAccrualReportAction() throws PersistenceException {
        // Remove the comment from the get method below if you want to display Table 4 in the totalAccrualReport.xhtml code.
        // The following method is used to show how the dataModel and columnNames are created from the raw database resultset for use in the Primefaces
        // dynamic column names dataTable code.
//        this.dataModel = this.getAccrualBySiteByMonthRawDatabaseRetrievalReportItems();
//// The following this.createParticipantFirstPaymentStatus method was added to start testing the First Payment Status software.
////        this.createParticipantFirstPaymentStatus();
        return "/totalAccrualReport.xhtml?faces-redirect=true";
    }

    // The MonthlyAccrualReportAction() method is called from the template.xhtml sub-menu "Monthly Accrual Report" under the main menu "ECP Reports".
    // Return the newly created monthlyAccuralReport.xhtml to the browswer for display, but the dataModelEcpTreatment, dataModelEcpObseration and columnNames need to be created first. 
    // The dataModel and columnNames used in the monthlyAccrualReport.xhtml are created by the createAccrualBySiteByMonthReportItems() method.
    public String MonthlyAccrualReportAction() throws PersistenceException {
        this.createAccrualBySiteByMonthReportItems();
        return "/monthlyAccrualReport.xhtml?faces-redirect=true";
    }
    
    //Methods for Current CRF Status Report
    public String CurrentCRFStatusReportAction() throws PersistenceException {
        this.createCurrentCRFStatusReportItems();
        return "/currentCRFStatusReport.xhtml?faces-redirect=true";
    }
    
    public void createCurrentCRFStatusReportItems() throws PersistenceException {
        ReportPersistenceManager rpm = ServiceRegistry.getReportPersistenceManager();  // Get the ReportPersistenceManager.
        // Use the ReportPersistenceManager getCurrentCRFStatus() method to create the dataModel and columnNames for the Primefaces dataTable.
        ReportItems reportItems = rpm.getCurrentCRFStatus();
        // Save the dataModel and columnNames locally after creation.
        this.dataModelCurrentCRFStatus = reportItems.getDataModel();
        this.columnNames = reportItems.getColumnNames();
    }
    
        
    public List<ReportItem> getAccrualReportItems() throws PersistenceException {
        List<ReportItem> totalAccruals = new ArrayList<>(5);
        ReportPersistenceManager rpm = ServiceRegistry.getReportPersistenceManager();
        totalAccruals = rpm.getTotalAccruals();

        return totalAccruals;
    }
    
    public void totalAccrualDownload() throws PersistenceException{
        List<ReportItem> totalAccruals = new ArrayList<>();
        ReportPersistenceManager rpm = ServiceRegistry.getReportPersistenceManager();
        totalAccruals = rpm.getTotalAccruals();

        List<ReportItem> totalAccrualsBySite = new ArrayList<>();
        totalAccrualsBySite = rpm.getTotalAccrualsBySite();


        try {
            String timestamp = new SimpleDateFormat("yyMMddHHmmssSSS").format(new Date());

            if (!Files.exists(Paths.get("/home/shared/WUSTL/ecp/rbos/TotalAccrualReport"))) {
                FileAttribute<Set<PosixFilePermission>> fa = PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("rwxr-x---"));

                Files.createDirectory(Paths.get("/home/shared/WUSTL/ecp/rbos/TotalAccrualReport"), fa);


            }

            if (!Files.exists(Paths.get("/home/shared/WUSTL/ecp/rbos/TotalAccrualReport/" + timestamp))) {
                FileAttribute<Set<PosixFilePermission>> fa = PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("rwxr-x---"));

                Files.createDirectory(Paths.get("/home/shared/WUSTL/ecp/rbos/TotalAccrualReport/" + timestamp), fa);


            }

            FileAttribute<Set<PosixFilePermission>> fa = PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("rw-r-----"));
            Files.createFile(Paths.get("/home/shared/WUSTL/ecp/rbos/TotalAccrualReport/" + timestamp + "/totalaccrual"+timestamp+".xlsx"), fa);



            FilledWorkbook fw = new FilledWorkbook("totalAccrual");
            fw.generateTotalAccrual(totalAccruals, totalAccrualsBySite);

            fw.publishFile("/home/shared/WUSTL/ecp/rbos/TotalAccrualReport/"+timestamp+"/totalaccrual"+timestamp+".xlsx");


            downloadXlsx(new File("/home/shared/WUSTL/ecp/rbos/TotalAccrualReport/"+timestamp+"/totalaccrual"+timestamp+".xlsx"));

            try {
                Thread.sleep(3000); // Letting the nsf files catch up to the closed streams so it can delete its lock files
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }

            Files.delete(Paths.get("/home/shared/WUSTL/ecp/rbos/TotalAccrualReport/"+timestamp+"/totalaccrual"+timestamp+".xlsx"));
            Files.delete(Paths.get("/home/shared/WUSTL/ecp/rbos/TotalAccrualReport/"+timestamp));

        } catch (IOException e){
            logger.error("IOException", e);

        }



    }

    public List<ReportItem> getAccrualLast30DaysReportItems() throws PersistenceException {
        List<ReportItem> totalAccrualsLast30Days = new ArrayList<>(5);
        ReportPersistenceManager rpm = ServiceRegistry.getReportPersistenceManager();
        totalAccrualsLast30Days = rpm.getTotalAccrualsLast30Days();

        return totalAccrualsLast30Days;
    }
    
    public List<ReportItem> getAccrualBySiteReportItems() throws PersistenceException {
        List<ReportItem> totalAccrualsBySite = new ArrayList<>(5);
        ReportPersistenceManager rpm = ServiceRegistry.getReportPersistenceManager();
        totalAccrualsBySite = rpm.getTotalAccrualsBySite();

        return totalAccrualsBySite;
    }
    
    // Create the dataModelEcpTreatment and columNames and save them here in the ReportsController to be called by the totalAccrualReport.xhtml Primefaces Table 4 making code. 
    // Table 4 is currently commented out in the totalAccrualReport.xhtml code.
    public DataModel getAccrualBySiteByMonthRawDatabaseRetrievalReportItems() throws PersistenceException {
        ReportItems reportItems = new ReportItems();
        ReportPersistenceManager rpm = ServiceRegistry.getReportPersistenceManager();
        reportItems = rpm.getTotalAccrualsBySiteByMonthRawDatabaseRetrieval();
        this.dataModelEcpTreatmentArm = reportItems.getDataModel();
        this.columnNames = reportItems.getColumnNames();
        return reportItems.getDataModel();
    }

    // The createAccrualBySiteByMonthReportItems method is used to create the dataModelEcpTreatment and columNames.
    // It saves both of them here in the ReportsController to be called by the monthlyAccrualReport.xhtml Primefaces table making code. 
    public void createAccrualBySiteByMonthReportItems() throws PersistenceException {
//        ReportItems reportItems = new ReportItems();
        ReportPersistenceManager rpm = ServiceRegistry.getReportPersistenceManager();  // Get the ReportPersistenceManager.
        // Use the ReportPersistenceManager getTotalAccrualsBySiteByMonth() method to create the dataModel and columnNames for the Primefaces dataTable.
        ReportItems reportItems = rpm.getTotalAccrualsBySiteByMonth("both_arms");
        // Save the dataModelEcpTreatment and columnNames locally after creation.
        this.dataModelEcpTreatmentArm = reportItems.getDataModel();
        this.columnNames = reportItems.getColumnNames();

        // Use the ReportPersistenceManager getTotalAccrualsBySiteByMonth() method to create the dataModel and columnNames for the Primefaces dataTable.
        // Save the dataModelEcpObservation locally after creation.
        //this.dataModelObservationArm = reportItems.getDataModel();

    }

    public void monthlyAccrualDownload() throws  PersistenceException{
        ReportPersistenceManager rpm = ServiceRegistry.getReportPersistenceManager();
        Map<String,ArrayList<Map<String, Object>>> reportItems = rpm.getTotalAccrualsBySiteByMonthByYear("both_arms");


        try {
            String timestamp = new SimpleDateFormat("yyMMddHHmmssSSS").format(new Date());

            if (!Files.exists(Paths.get("/home/shared/WUSTL/ecp/rbos/MonthlyAccrualReport"))) {
                FileAttribute<Set<PosixFilePermission>> fa = PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("rwxr-x---"));

                Files.createDirectory(Paths.get("/home/shared/WUSTL/ecp/rbos/MonthlyAccrualReport"), fa);


            }

            if (!Files.exists(Paths.get("/home/shared/WUSTL/ecp/rbos/MonthlyAccrualReport/" + timestamp))) {
                FileAttribute<Set<PosixFilePermission>> fa = PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("rwxr-x---"));

                Files.createDirectory(Paths.get("/home/shared/WUSTL/ecp/rbos/MonthlyAccrualReport/" + timestamp), fa);


            }

            FileAttribute<Set<PosixFilePermission>> fa = PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("rw-r-----"));
            Files.createFile(Paths.get("/home/shared/WUSTL/ecp/rbos/MonthlyAccrualReport/" + timestamp + "/monthlyaccrual"+timestamp+".xlsx"), fa);



            FilledWorkbook fw = new FilledWorkbook("monthlyAccrual");
            fw.generateMonthlyAccrual(reportItems);
            fw.publishFile("/home/shared/WUSTL/ecp/rbos/MonthlyAccrualReport/"+timestamp+"/monthlyaccrual"+timestamp+".xlsx");


            downloadXlsx(new File("/home/shared/WUSTL/ecp/rbos/MonthlyAccrualReport/"+timestamp+"/monthlyaccrual"+timestamp+".xlsx"));

            try {
                Thread.sleep(3000); // Letting the nsf files catch up to the closed streams so it can delete its lock files
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }

            Files.delete(Paths.get("/home/shared/WUSTL/ecp/rbos/MonthlyAccrualReport/"+timestamp+"/monthlyaccrual"+timestamp+".xlsx"));
            Files.delete(Paths.get("/home/shared/WUSTL/ecp/rbos/MonthlyAccrualReport/"+timestamp));
            this.monthlyAccrualOutcome = "Download completed successfully";

        } catch (IOException e){
            logger.error("IOException", e);

        }


    }

    public void copyFilesAction() throws PersistenceException, IOException {
        String date = convertDateToValidString(this.crfInputDate, "yyyy-MM-dd");

        Calendar c =  Calendar.getInstance();
        c.setTime(this.crfInputDate);
        c.add(c.DATE,-1);

        String month = getMonth(c.getTime());
        String timestamp = new SimpleDateFormat("yyMMddHHmmssSSS").format(new Date()) + "RBOSReport";
        String dayBefore = getDayBefore(this.crfInputDate);
        try {
            ReportPersistenceManager rpm = ServiceRegistry.getReportPersistenceManager();
            List<Site> sites = rpm.getSites();
            HashMap<String, Integer> nameToId = new HashMap<>();

            for (Site site : sites) {
                if (site.getId() > 1) {
                    nameToId.put(site.getName() + "" + month + "CRFReport" + timestamp, site.getSiteID());
                }
            }

            //If the MonthlySiteReportsRBOS directory doesn't already exist, this snippet creates a new directory.
            if (!Files.exists(Paths.get("/home/shared/WUSTL/ecp/rbos/MonthlySiteReportsRBOS"))) {
                FileAttribute<Set<PosixFilePermission>> fa = PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("rwxr-x---"));

                Files.createDirectory(Paths.get("/home/shared/WUSTL/ecp/rbos/MonthlySiteReportsRBOS"), fa);


            }

            if (!Files.exists(Paths.get("/home/shared/WUSTL/ecp/rbos/MonthlySiteReportsRBOS/" + timestamp))) {
                FileAttribute<Set<PosixFilePermission>> fa = PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("rwxr-x---"));

                Files.createDirectory(Paths.get("/home/shared/WUSTL/ecp/rbos/MonthlySiteReportsRBOS/" + timestamp), fa);


            }

            for (String name : nameToId.keySet()) {
                File csv = new File("/home/shared/WUSTL/ecp/rbos/MonthlySiteReportsRBOS/" + timestamp + "/" + name + ".xlsx");
                FileAttribute<Set<PosixFilePermission>> fa = PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("rw-r-----"));
                Files.createFile(Paths.get("/home/shared/WUSTL/ecp/rbos/MonthlySiteReportsRBOS/" + timestamp + "/" + name + ".xlsx"), fa);

            }
            for (String name : nameToId.keySet()) {
                rpm.runCRFCopy(name, nameToId.get(name), date, dayBefore, timestamp);
            }

            zipDirectory(nameToId.keySet(), timestamp, "/home/shared/WUSTL/ecp/rbos/MonthlySiteReportsRBOS/"+timestamp);

            downloadZip(new File("/home/shared/WUSTL/ecp/rbos/MonthlySiteReportsRBOS/" + timestamp + ".zip"));

            //emailZip(timestamp); No longer used due to security reasons (must encrypt email)
            try {
                Thread.sleep(3000); // Letting the nsf files catch up to the closed streams so it can delete its lock files
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            cleanTMP(timestamp);
            this.crfOutcome = "Download completed successfully";
        }catch(PersistenceException e){
            logger.error("PersistenceException", e);

        }catch(IOException ex) {
            logger.error("IOException", ex);
        }
    }

    public void cleanTMP(String timestamp){
        File f = new File("/home/shared/WUSTL/ecp/rbos/MonthlySiteReportsRBOS/"+timestamp);
        File[] files = f.listFiles();

        for (File file: files){
            file.delete();
        }
        f.delete();

        File zip = new File("/home/shared/WUSTL/ecp/rbos/MonthlySiteReportsRBOS/"+timestamp+".zip");
        zip.delete();
    }

    public void zipDirectory(Set<String> files, String timestamp, String path) {
        ZipOutputStream zos = null;
        BufferedInputStream bis = null;

        try {
            logger.audit("files" + files.toString());
            zos = new ZipOutputStream(new FileOutputStream(path + ".zip"));

        for (String name : files) {

            ZipEntry ze = new ZipEntry(name + ".xlsx");
            zos.putNextEntry(ze);
            File f = new File(path+"/" + name +".xlsx");

            bis = new BufferedInputStream(new FileInputStream(
                    f));

            long bytesRead = 0;
            byte[] bytesIn = new byte[4096];
            int read = 0;
            while ((read = bis.read(bytesIn)) != -1) {
                zos.write(bytesIn, 0, read);
                bytesRead += read;
            }

            zos.closeEntry();
            bis.close();
            logger.audit("zos" + zos.toString());

        }

    }catch(IOException ex){
        logger.error("IOException", ex);
    }finally {
            try {
                if (Objects.nonNull(zos)) {
                    zos.close();
                }
                if (Objects.nonNull(bis)) {
                    bis.close();
                }
            }catch(IOException e){
                logger.error("IOException", e);
            }
        }

    }

    // Once all new reports have been merged, merge this method with download zip.
    public void downloadXlsx(File f){
        OutputStream ros = null;
        FileInputStream fis = null;
        try{
            FacesContext fc = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
            String fileType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            response.reset();
            response.setContentType(fileType);

            response.setHeader("Content-Disposition","attachment; filename="+f.getName());

            fis = new FileInputStream(f);
            ros = response.getOutputStream();

            long bytesRead = 0;
            byte[] bytesIn = new byte[4096];
            int c;
            while ((c = fis.read(bytesIn)) != -1){
                ros.write(bytesIn, 0, c);
                bytesRead += c;
            }

            ros.flush();
            ros.close();

            fis.close();

            fc.responseComplete();

        }catch (IOException e){
            logger.error("IOException", e);
        }
        finally {
            try {
                if (fis != null) {
                    fis.close();
                }
                if (ros != null) {
                    ros.close();
                }
            }catch(IOException e){
                logger.error("IOException", e);
            }
        }

    }

    public void downloadZip(File f){
        OutputStream ros = null;
        FileInputStream fis = null;
        try{
            FacesContext fc = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
            String fileType = "application/zip";

            response.reset();
            response.setContentType(fileType);

            response.setHeader("Content-Disposition","attachment; filename="+f.getName());

            fis = new FileInputStream(f);
            ros = response.getOutputStream();

            long bytesRead = 0;
            byte[] bytesIn = new byte[4096];
            int c;
            while ((c = fis.read(bytesIn)) != -1){
                ros.write(bytesIn, 0, c);
                bytesRead += c;
            }

            ros.flush();
            ros.close();

            fis.close();

            fc.responseComplete();

        }catch (IOException e){
            logger.error("IOException", e);
        }
        finally {
            try {
                if (fis != null) {
                    fis.close();
                }
                if (ros != null) {
                    ros.close();
                }
            }catch(IOException e){
                logger.error("IOException", e);
            }
        }


    }

    //UNUSED!! The previous usage contained PHI
    // So make sure that if you use this method (or the sendWithAttachment() from Postman), that the email does not have ANY PHI
    // We would need to encrypt the files if they did contain PHI
   /* public void emailZip(String timestamp) throws NamingException {

        Context env = (Context) new InitialContext().lookup("java:comp/env");
        String smtphost = (String)env.lookup("smtpHost");

        Postman sendMail = new Postman( smtphost);

        try {
            sendMail.addRecipient(new InternetAddress("commeanp@wustl.edu"));
            sendMail.addRecipient( new InternetAddress("derflerm@wustl.edu"));
            sendMail.addRecipient( new InternetAddress("ktwichell@wustl.edu"));

            sendMail.setFrom( new InternetAddress("notify@mir.wustl.edu"));
            sendMail.setSubject("Monthly Site Reports RBOS for " + getMonth(this.crfInputDate));

            MimeBodyPart attachment = new MimeBodyPart();

            attachment.setFileName("MonthlySiteReportsRBOS.zip");
            attachment.attachFile(new File("/home/shared/WUSTL/ecp/rbos/MonthlySiteReportsRBOS/"+timestamp+".zip"));

            MimeMultipart mp = new MimeMultipart();
            mp.addBodyPart(attachment);



            sendMail.sendWithAttachment(mp);

        } catch (AddressException ex) {
            ex.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }*/


    //Converts Date object into String needed in queries
    public String convertDateToValidString(Date date, String pattern){
        DateFormat format = new SimpleDateFormat(pattern);
        return format.format(date);
    }

    public String getMonth(Date date){
        String pattern = "MMMMM";
        DateFormat format = new SimpleDateFormat(pattern);
        return format.format(date);
    }

    public String getDayBefore(Date date){
        Calendar c =  Calendar.getInstance();
        c.setTime(date);
        c.add(c.DATE,-1);
        return convertDateToValidString(c.getTime(), "yyyy-MM-dd");

    }

    public String getYear(Date date){
        String pattern = "yyyy";
        DateFormat format = new SimpleDateFormat(pattern);
        return format.format(date);
    }

    public void createGeorgeReport(){
        try {
            String timestamp = new SimpleDateFormat("yyMMddHHmmssSSS").format(new Date());

            if (!Files.exists(Paths.get("/home/shared/WUSTL/ecp/rbos/GeorgeReport"))) {
                FileAttribute<Set<PosixFilePermission>> fa = PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("rwxr-x---"));

                Files.createDirectory(Paths.get("/home/shared/WUSTL/ecp/rbos/GeorgeReport"), fa);


            }

            if (!Files.exists(Paths.get("/home/shared/WUSTL/ecp/rbos/GeorgeReport/" + timestamp))) {
                FileAttribute<Set<PosixFilePermission>> fa = PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("rwxr-x---"));

                Files.createDirectory(Paths.get("/home/shared/WUSTL/ecp/rbos/GeorgeReport/" + timestamp), fa);


            }
            String year = this.projectionInputYear;

            FileAttribute<Set<PosixFilePermission>> fa = PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("rw-r-----"));
            Files.createFile(Paths.get("/home/shared/WUSTL/ecp/rbos/GeorgeReport/" + timestamp + "/georgereport"+year+".xlsx"), fa);

            ReportPersistenceManager rpm = ServiceRegistry.getReportPersistenceManager();
            HashMap<String, Integer> siteToCount = rpm.getEnrolledParticipantsCountYearRestriction(year);

            FilledWorkbook fw = new FilledWorkbook("george");
            fw.generateGeorge(siteToCount, year);
            fw.publishFile("/home/shared/WUSTL/ecp/rbos/GeorgeReport/"+timestamp+"/georgereport"+year+".xlsx");

            Set<String> files = new HashSet<>();
            files.add("georgereport" + year);
            zipDirectory(files, timestamp, "/home/shared/WUSTL/ecp/rbos/GeorgeReport/"+timestamp);

            downloadZip(new File("/home/shared/WUSTL/ecp/rbos/GeorgeReport/"+timestamp+".zip"));

            try {
                Thread.sleep(3000); // Letting the nsf files catch up to the closed streams so it can delete its lock files
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }

            Files.delete(Paths.get("/home/shared/WUSTL/ecp/rbos/GeorgeReport/"+timestamp+".zip"));
            Files.delete(Paths.get("/home/shared/WUSTL/ecp/rbos/GeorgeReport/"+timestamp+"/georgereport"+year+".xlsx"));
            Files.delete(Paths.get("/home/shared/WUSTL/ecp/rbos/GeorgeReport/"+timestamp));
            this.projectionOutcome = "Download completed successfully";

        }catch(PersistenceException e){
            logger.error("PersistenceException", e);
        }catch (IOException e){
            logger.error("IOException", e);

        }
    }

    public String createParticipantFirstPaymentStatus() throws PersistenceException {
        
        Integer formCount = 0;
        int siteid;
        String pid;
        String eventName;
        
        ReportPersistenceManager rpm = ServiceRegistry.getReportPersistenceManager();
        
        siteid = 2;
        pid = "101001";
        eventName = "Confirmation of Eligibility";
        
        formCount = rpm.getParticipantFirstPaymentStatus(siteid, pid, eventName);
        System.out.println("The form count for event named " + eventName + " is: " + formCount + "\n");

        eventName = "ECP Treatment 1";
        
        formCount = rpm.getParticipantFirstPaymentStatus(siteid, pid, eventName);
        System.out.println("The form count for event named " + eventName + " is: " + formCount + "\n");

        return formCount.toString();
    }

    //Gets the total number of participants in the RBOS ECP that have not been treated
    //All of the other fetch methods do something similar with either different queries or status requirements
    //The method query below does return patients without reported ECP treatments so an additional query is needed (getEnrollmentCRFSpecial())
    public int fetchECPNotTreated(Date date) throws PersistenceException {
        ReportPersistenceManager rpm = ServiceRegistry.getReportPersistenceManager();
        String fd = convertDateToValidString(date, "yyyy-MM-dd");
        String sql = "select p.id, p.siteid, p.participantid, p.enrolleddate, p.studyarmstatus, p.overridedate, e.id, e.participantid, e.name, e.status from participant as p, event as e where p.id = e.participantid and p.siteid > 1 and  studyarmstatus = 'ECP_TREATMENT_ARM' and e.name = 'ECP Treatment 1' and enrolleddate < '"+ fd +"' and p.overridedate is null order by e.status;";

        String[] statuses = {"NEW", "NOT_REQUIRED"};
        int count = rpm.getEnrollment(sql, statuses);
        int additionalCRF = rpm.getEnrollmentCRFSpecial(fd);
        return (count + additionalCRF);
    }
    public int fetchECPTreated(Date date) throws PersistenceException {
        ReportPersistenceManager rpm = ServiceRegistry.getReportPersistenceManager();
        String fd = convertDateToValidString(date, "yyyy-MM-dd");
        String sql = "select p.id, p.siteid, p.participantid, p.enrolleddate, p.studyarmstatus, p.overridedate, e.id, e.participantid, e.name, e.status from participant as p, event as e where p.id = e.participantid and p.siteid > 1 and  studyarmstatus = 'ECP_TREATMENT_ARM' and e.name = 'ECP Treatment 1' and enrolleddate < '"+ fd +"' and p.overridedate is null order by e.status;";

        String[] statuses = {"SUBMITTED", "DCC_VERIFIED", "STARTED", "CRF_QUERY", "PI_APPROVED"};
        int count = rpm.getEnrollment(sql, statuses);
        return count;
    }

    public int fetchObsTreated(Date date) throws PersistenceException {
        ReportPersistenceManager rpm = ServiceRegistry.getReportPersistenceManager();
        String fd = convertDateToValidString(date, "yyyy-MM-dd");
        String sql =  "select p.id, p.siteid, p.participantid, p.enrolleddate, p.studyarmstatus, p.overridedate, e.id, e.participantid, e.name, e.status from participant as p, event as e where p.id = e.participantid and p.siteid > 1 and  p.studyarmstatus = 'ECP_TREATMENT_ARM' and e.name = 'ECP Treatment 1' and p.enrolleddate < '"+ fd + "' and p.overridedate is not null order by e.status";

        String[] statuses = {"SUBMITTED", "DCC_VERIFIED", "STARTED", "CRF_QUERY", "PI_APPROVED"};
        int count = rpm.getEnrollment(sql, statuses);
        return count;
    }
    public int fetchObsNotTreated(Date date) throws PersistenceException {
        ReportPersistenceManager rpm = ServiceRegistry.getReportPersistenceManager();
        String fd = convertDateToValidString(date, "yyyy-MM-dd");
        String sql =  "select p.id, p.siteid, p.participantid, p.enrolleddate, p.studyarmstatus, p.overridedate, e.id, e.participantid, e.name, e.status from participant as p, event as e where p.id = e.participantid and p.siteid > 1 and  p.studyarmstatus = 'ECP_TREATMENT_ARM' and e.name = 'ECP Treatment 1' and p.enrolleddate < '"+ fd + "' and p.overridedate is not null order by e.status";

        String[] statuses = {"NEW", "NOT_REQUIRED"};
        int count = rpm.getEnrollment(sql, statuses);
        return count;
    }
    public int fetchObsNoCrossover(Date date) throws PersistenceException {
        ReportPersistenceManager rpm = ServiceRegistry.getReportPersistenceManager();
        String fd = convertDateToValidString(date,"yyyy-MM-dd");
        String sql = "select p.id, p.siteid, p.participantid, p.enrolleddate, p.studyarmstatus, p.overridedate, e.id, e.participantid, e.name, e.status from participant as p, event as e where p.id = e.participantid and p.siteid > 1 and  studyarmstatus = 'OBSERVATIONAL_ARM' and e.name = 'Confirmation of Eligibility' and enrolleddate < '"+ fd +"' order by e.status";
        String[] statuses = {"DCC_VERIFIED", "CRF_QUERY", "NEW", "NOT_REQUIRED", "STARTED", "SUBMITTED", "PI_APPROVED"};
        int count = rpm.getEnrollment(sql, statuses);
        return count;
    }


}
