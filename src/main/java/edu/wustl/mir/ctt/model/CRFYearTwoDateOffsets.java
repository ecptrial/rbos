/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.wustl.mir.ctt.model;

/**
 *
 * @author Lauren Wallace
 */
public class CRFYearTwoDateOffsets {
    private final int[] currentTherapyDateOffsets = {0, 820, 910, 1000, 1095};
    private final String[] currentTherapyNames = {"", "Year Three Day 90", "Year Three Day 180", "Year Three Day 270", "Year Three Day 365"};

    public int[] getCurrentTherapyDateOffsets() {
        return currentTherapyDateOffsets;
    }
    
    public int getCurrentTherapyDateOffset(int i) {
        return currentTherapyDateOffsets[i];
    }
    
    public String getCurrentTherapyName(int i) {
        return currentTherapyNames[i];
    }

}
