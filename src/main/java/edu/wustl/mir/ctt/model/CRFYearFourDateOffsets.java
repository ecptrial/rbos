/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.wustl.mir.ctt.model;

/**
 *
 * @author Lauren Wallace
 */
public class CRFYearFourDateOffsets {
    private final int[] currentTherapyDateOffsets = {0, 1550, 1640, 1730, 1825};
    private final String[] currentTherapyNames = {"", "Year Five Day 90", "Year Five Day 180", "Year Five Day 270", "Year Five Day 365"};

    public int[] getCurrentTherapyDateOffsets() {
        return currentTherapyDateOffsets;
    }
    
    public int getCurrentTherapyDateOffset(int i) {
        return currentTherapyDateOffsets[i];
    }
    
    public String getCurrentTherapyName(int i) {
        return currentTherapyNames[i];
    }

}
