/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.wustl.mir.ctt.model;

/**
 *
 * @author Lauren Wallace
 */
public class CRFSixMonthDateOffsets {
    private final int[] pulmEvalDateOffsets      = {0, 210, 240, 270, 300, 330, 365};
    private final int[] pulmEvalLastDateOffsets  = {0,  31,  31,  31,  31,  31,  31};
    private final int[] changeTherapyDateOffsets = {0, 210, 240, 270, 300, 330, 365};
    private final int[] qualityOfLifeDateOffsets = {0, 270, 365};
    
    public int[] getPulmEvalDateOffsets() {
        return pulmEvalDateOffsets;
    }

    public int getPulmEvalDateOffset(int i){
        return pulmEvalDateOffsets[i];
    }
    
    public int[] getPulmEvalLastDateOffsets() {
        return pulmEvalLastDateOffsets;
    }
    
    public int getPulmEvalLastDateOffset(int i) {
        return pulmEvalLastDateOffsets[i];
    }
    
    public int[] getChangeTherapyDateOffsets() {
        return changeTherapyDateOffsets;
    }
    
    public int getChangeTherapyDateOffset(int i) {
        return changeTherapyDateOffsets[i];
    }
    
    public int[] getQualityOfLifeDateOffsets() {
        return qualityOfLifeDateOffsets;
    }
    
    public int getQualityOfLifeDateOffset(int i) {
        return qualityOfLifeDateOffsets[i];
    }
}
