package edu.wustl.mir.ctt.model;

import edu.wustl.mir.ctt.persistence.PersistenceException;
import edu.wustl.mir.ctt.persistence.PersistenceManager;
import edu.wustl.mir.ctt.persistence.ServiceRegistry;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author drm
 */
public class PIDGeneratorECP {
    
    public static String getNewParticipantID( Site s) throws PersistenceException {
        String pid = null;
        
        PersistenceManager pm = ServiceRegistry.getPersistenceManager();
        List<String> pids = pm.getAllParticipantIDs(s);
        
        // Remove participant IDs of participants moved in from another site
        // so that they are not counted towards next participant number
        // for this site, use iterator to avoid concurrent modification exception
        String siteId = Integer.toString(s.getSiteID());
        
        for (Iterator<String> itr = pids.iterator(); itr.hasNext();) {
            String thisPid = itr.next();
            
            if (!thisPid.startsWith(siteId)) {
                itr.remove();
            }
        }
        
        // This preexisting logic should prevent duplicate assignment of a
        // number by incrementing past it if the collection contains it
        int i = 1;
        do {
            pid = Integer.toString(s.getSiteID() * 1000 + pids.size() + i);
            i++;
        } while( pids.contains(pid));
        
        return pid;
    }
}
