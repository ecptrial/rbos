package edu.wustl.mir.ctt.model;

public enum CRFStatus {
    INITIAL,    // First six months unlocked
    YEAR_ONE,   // All year one forms unlocked
    YEAR_TWO,   // All year two forms unlocked
    YEAR_THREE, // All year three forms unlocked
    YEAR_FOUR,  // All year four forms unlocked
    COMPLETED,  // All forms unlocked (or early termination)
    UNKNOWN;    // Unknown dummystatus

    /**
     * Return matching enum based on string.
     * 
     * The enum is stored in an RDBMS as string. This allows the conversion back
     * to enum from the DB's stored string.
     * 
     * @param s
     * @return 
     */
    // Constructor for the enum.  This constructor is called once for each initializer above.
    public static CRFStatus getStatus(String s) {
        if("INITIAL".equals(s))
            return INITIAL;
        else if("YEAR_ONE".equals(s))
            return YEAR_ONE;
        else if("YEAR_TWO".equals(s))
            return YEAR_TWO;
        else if( "YEAR_THREE".equals(s))
            return YEAR_THREE;
        else if( "YEAR_FOUR".equals(s))
            return YEAR_FOUR;
        else if( "COMPLETED".equals(s))
            return COMPLETED;
        else
            return UNKNOWN;
    }
}
