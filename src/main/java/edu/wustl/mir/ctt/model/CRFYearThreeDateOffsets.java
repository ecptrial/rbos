/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.wustl.mir.ctt.model;

/**
 *
 * @author Lauren Wallace
 */
public class CRFYearThreeDateOffsets {
    private final int[] currentTherapyDateOffsets = {0, 1185, 1275, 1365, 1460};
    private final String[] currentTherapyNames = {"", "Year Four Day 90", "Year Four Day 180", "Year Four Day 270", "Year Four Day 365"};

    public int[] getCurrentTherapyDateOffsets() {
        return currentTherapyDateOffsets;
    }
    
    public int getCurrentTherapyDateOffset(int i) {
        return currentTherapyDateOffsets[i];
    }
    
    public String getCurrentTherapyName(int i) {
        return currentTherapyNames[i];
    }

}
