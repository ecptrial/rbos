/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.wustl.mir.ctt.model;

import java.util.Date;

/**
 *
 * @author lwalla01
 */
public class StatusChangeEvent {
    private int id;
    private int formId;
    private FormStatus oldStatus;
    private FormStatus newStatus;
    private String username;
    private Date changeDate;
    private String source;
    private String comment;
    
    public StatusChangeEvent(int f, FormStatus o, FormStatus n, String u, Date cd, String s, String c) {
        formId = f;
        oldStatus = o;
        newStatus = n;
        username = u;
        changeDate = cd;
        source = s;
        comment = c;
    }
    
    public int getId() { return id; }
    public int getFormId() { return formId; }
    public FormStatus getOldStatus() { return oldStatus; }
    public FormStatus getNewStatus() { return newStatus; }
    public String getUsername() { return username; }
    public Date getChangeDate() { return changeDate; }
    public String getSource() { return source; }
    public String getComment() { return comment; }

    public void setId(int i) { id = i; }
    public void setFormId(int f) { formId = f; }
    public void setOldStatus(FormStatus o) { oldStatus = o; }
    public void setNewStatus(FormStatus n) { newStatus = n; }
    public void setUsername(String u) { username = u; }
    public void setChangeDate(Date s) { changeDate = s; }
    public void setSource(String s) { source = s; }
    public void setComment(String c) { comment = c; }
}
