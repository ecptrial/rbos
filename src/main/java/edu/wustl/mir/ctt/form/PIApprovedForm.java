package edu.wustl.mir.ctt.form;

import edu.wustl.mir.ctt.model.AttributeBoolean;
import edu.wustl.mir.ctt.model.AttributeDate;
import edu.wustl.mir.ctt.model.AttributeString;
import edu.wustl.mir.ctt.model.ECPEventTypes;
import edu.wustl.mir.ctt.model.ECPFormTypes;
import edu.wustl.mir.ctt.model.VerificationStatus;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Lauren Wallace
 */
public class PIApprovedForm extends BasicForm {
    
    public static final String[] SourceDocumentTypes = new String[]{"Signed PI Approved CRF"};

    public PIApprovedForm() {
        // constructor
        super();
        formType = ECPFormTypes.PI_APPROVED; // Form
        title = "PI Approved Form";
        this.sourceDocumentTypes = SourceDocumentTypes;
        
        attributes.put("reviewedDate", new AttributeDate("reviewedDate"));        
        attributes.put("coeReviewed", new AttributeBoolean("coeReviewed"));
        attributes.put("dmhReviewed", new AttributeBoolean("dmhReviewed"));
        attributes.put("btReviewed", new AttributeBoolean("btReviewed"));
        attributes.put("citReviewed", new AttributeBoolean("citReviewed"));
        attributes.put("ctReviewed", new AttributeBoolean("ctReviewed"));
        attributes.put("ecpReviewed", new AttributeBoolean("ecpReviewed"));
        attributes.put("peReviewed", new AttributeBoolean("peReviewed"));
        attributes.put("cscReviewed", new AttributeBoolean("cscReviewed"));
        attributes.put("afuReviewed", new AttributeBoolean("afuReviewed"));
        attributes.put("saeReviewed", new AttributeBoolean("saeReviewed"));
        attributes.put("qolReviewed", new AttributeBoolean("qolReviewed"));
        attributes.put("eosReviewed", new AttributeBoolean("eosReviewed"));
        attributes.put("pfuReviewed", new AttributeBoolean("pfuReviewed"));
        
        attributes.put("comment", new AttributeString("comment"));
        
        attributes.put("piSignature", new AttributeString("piSignature"));

       this.clear();

    }
    
    public PIApprovedForm( BasicForm bf) {
        super(bf);
        formType = ECPFormTypes.PI_APPROVED; // Form
        title = bf.getTitle();
        this.sourceDocumentTypes = SourceDocumentTypes;
    }
	
    public Date getReviewedDate() {
        return (Date) attributes.get("reviewedDate").getValue();
    }
    
    public void setReviewedDate(Date rDate) {
        attributes.get("reviewedDate").setValue(rDate);
    }

    public boolean isCOEReviewed() {
        return (boolean) attributes.get("coeReviewed").getValue();
    }
    
    public void setCOEReviewed( boolean b) {
        attributes.get("coeReviewed").setValue(b);
    }

    public boolean isDMHReviewed() {
        return (boolean) attributes.get("dmhReviewed").getValue();
    }
    
    public void setDMHReviewed( boolean b) {
        attributes.get("dmhReviewed").setValue(b);
    }
    
    public boolean isBTReviewed() {
        return (boolean) attributes.get("btReviewed").getValue();
    }
    
    public void setBTReviewed( boolean b) {
        attributes.get("btReviewed").setValue(b);
    }
    
    public boolean isCITReviewed() {
        return (boolean) attributes.get("citReviewed").getValue();
    }
    
    public void setCITReviewed( boolean b) {
        attributes.get("citReviewed").setValue(b);
    }
    
    public boolean isCTReviewed() {
        return (boolean) attributes.get("ctReviewed").getValue();
    }
    
    public void setCTReviewed( boolean b) {
        attributes.get("ctReviewed").setValue(b);
    }
    
    public boolean isECPReviewed() {
        return (boolean) attributes.get("ecpReviewed").getValue();
    }
    
    public void setECPReviewed( boolean b) {
        attributes.get("ecpReviewed").setValue(b);
    }
    
    public boolean isPEReviewed() {
        return (boolean) attributes.get("peReviewed").getValue();
    }
    
    public void setPEReviewed( boolean b) {
        attributes.get("peReviewed").setValue(b);
    }
    
    public boolean isCSCReviewed() {
        return (boolean) attributes.get("cscReviewed").getValue();
    }
    
    public void setCSCReviewed( boolean b) {
        attributes.get("cscReviewed").setValue(b);
    }
    
    public boolean isAFUReviewed() {
        return (boolean) attributes.get("afuReviewed").getValue();
    }
    
    public void setAFUReviewed( boolean b) {
        attributes.get("afuReviewed").setValue(b);
    }
    
    public boolean isSAEReviewed() {
        return (boolean) attributes.get("saeReviewed").getValue();
    }
    
    public void setSAEReviewed( boolean b) {
        attributes.get("saeReviewed").setValue(b);
    }
    
    public boolean isQOLReviewed() {
        return (boolean) attributes.get("qolReviewed").getValue();
    }
    
    public void setQOLReviewed( boolean b) {
        attributes.get("qolReviewed").setValue(b);
    }
    
    public boolean isEOSReviewed() {
        return (boolean) attributes.get("eosReviewed").getValue();
    }
    
    public void setEOSReviewed( boolean b) {
        attributes.get("eosReviewed").setValue(b);
    }
    
    public boolean isPFUReviewed() {
        return (boolean) attributes.get("pfuReviewed").getValue();
    }
    
    public void setPFUReviewed( boolean b) {
        attributes.get("pfuReviewed").setValue(b);
    }
    
    public String getComment() {
        return (String) attributes.get("comment").getValue();
    }
    
    public void setComment(String comment) {
        attributes.get("comment").setValue(comment);
    }
    
    public String getPISignature() {
        return (String) attributes.get("piSignature").getValue();
    }
    
    public void setPISignature(String pisignature) {
        attributes.get("piSignature").setValue(pisignature);
    }
    
    public List<ECPEventTypes> getApprovedTypes() {
        List<ECPEventTypes> approvedTypes = new ArrayList<>();
        
        if (isCOEReviewed()) {
            approvedTypes.add(ECPEventTypes.ELIGIBILITY);
        }
        
        if (isDMHReviewed()) {
            approvedTypes.add(ECPEventTypes.DEMOGRAPHICS);
        }
        
        if (isBTReviewed()) {
            approvedTypes.add(ECPEventTypes.BASELINE_THERAPY);
        }
        
        if (isCITReviewed()) {
            approvedTypes.add(ECPEventTypes.CHANGE_THERAPY);
        }
        
        if (isCTReviewed()) {
            approvedTypes.add(ECPEventTypes.CURRENT_THERAPY);
        }
        
        if (isECPReviewed()) {
            approvedTypes.add(ECPEventTypes.ECP_TREATMENT);
        }
        
        if (isPEReviewed()) {
            approvedTypes.add(ECPEventTypes.PULMONARY_EVAL);
        }
        
        if (isCSCReviewed()) {
            approvedTypes.add(ECPEventTypes.CROSSOVER_SAFETY_CHECK);
        }
        
        if (isAFUReviewed()) {
            approvedTypes.add(ECPEventTypes.ANNUAL_FOLLOW_UP);
        }
        
        if (isSAEReviewed()) {
            approvedTypes.add(ECPEventTypes.SERIOUS_ADVERSE_EVENT);
        }
        
        if (isQOLReviewed()) {
            approvedTypes.add(ECPEventTypes.QUALITY_OF_LIFE);
        }
        
        if (isEOSReviewed()) {
            approvedTypes.add(ECPEventTypes.END_OF_STUDY);
        }
        
        if (isPFUReviewed()) {
            approvedTypes.add(ECPEventTypes.PARTICIPANT_FOLLOW_UP);
        }
        
        return approvedTypes;
    }
}
