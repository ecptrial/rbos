package edu.wustl.mir.ctt.form;

import edu.wustl.mir.ctt.model.AttributeDate;
import edu.wustl.mir.ctt.model.AttributeString;
import edu.wustl.mir.ctt.model.ECPFormTypes;
import edu.wustl.mir.ctt.model.VerificationStatus;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import edu.wustl.mir.ctt.log.AuditLogger;

/**
 *
 * @author pkc
 */
public class ParticipantFollowUpForm extends BasicForm {
    private transient AuditLogger logger = AuditLogger.create(ECPTreatmentForm.class);

    public static final String[] SourceDocumentTypes = new String[]{"ECP Report or Procedure Note","Hospice Report, Autopsy Report, or Death Summary, Clinic Note"};
    private List<Date> followUpECPTxDates;

    public ParticipantFollowUpForm() {
        // constructor
        super();
        formType = ECPFormTypes.PARTICIPANT_FOLLOW_UP;
        title = "V4&5 Participant Follow-Up Form";
        this.sourceDocumentTypes = SourceDocumentTypes;

        attributes.put("participantFollowUpDate", new AttributeDate("participantFollowUpDate"));        
        attributes.put("participantFollowUpReceivingECPTx", new AttributeString("participantFollowUpReceivingECPTx"));
        attributes.put("participantFollowUpReceivingFurtherECPTx", new AttributeString("participantFollowUpReceivingFurtherECPTx"));
        attributes.put("participantFollowUpECPTxDate1", new AttributeDate("participantFollowUpECPTxDate1", true, false, true));
        attributes.put("participantFollowUpECPTxDate2", new AttributeDate("participantFollowUpECPTxDate2"));
        attributes.put("participantFollowUpECPTxDate3", new AttributeDate("participantFollowUpECPTxDate3"));
        attributes.put("participantFollowUpECPTxDate4", new AttributeDate("participantFollowUpECPTxDate4"));
        attributes.put("participantFollowUpECPTxDate5", new AttributeDate("participantFollowUpECPTxDate5"));
        attributes.put("participantFollowUpECPTxDate6", new AttributeDate("participantFollowUpECPTxDate6"));
        attributes.put("participantFollowUpReceivingPFTs", new AttributeString("participantFollowUpReceivingPFTs"));
        attributes.put("participantAlive", new AttributeString("participantAlive"));
        attributes.put("participantFollowUpDateDeath", new AttributeDate("participantFollowUpDateDeath"));
        attributes.put("participantFollowUpCauseDeath", new AttributeString("participantFollowUpCauseDeath"));
        attributes.put("participantFollowUpOtherCauseDeath", new AttributeString("participantFollowUpOtherCauseDeath"));

        initFollowUpTxDates();

        this.clear();
    }
    
    public ParticipantFollowUpForm( BasicForm bf) {
        super(bf);		
        title = bf.getTitle();
        this.sourceDocumentTypes = SourceDocumentTypes;
        initFollowUpTxDates();
    }
	
    public Date getParticipantFollowUpDate() {
        return (Date) attributes.get("participantFollowUpDate").getValue();
    }
    
    public void setParticipantFollowUpDate(Date participantFollowUpDate) {
        attributes.get("participantFollowUpDate").setValue(participantFollowUpDate);
    }
    
    public String getParticipantFollowUpReceivingECPTx() {
        return (String) attributes.get("participantFollowUpReceivingECPTx").getValue();
    }
    
    public void setParticipantFollowUpReceivingECPTx(String participantFollowUpReceivingECPTx) {
        attributes.get("participantFollowUpReceivingECPTx").setValue(participantFollowUpReceivingECPTx);
    }
    
    public String getParticipantFollowUpReceivingFurtherECPTx() {
        return (String) attributes.get("participantFollowUpReceivingFurtherECPTx").getValue();
    }
    
    public void setParticipantFollowUpReceivingFurtherECPTx(String participantFollowUpReceivingFurtherECPTx) {
        attributes.get("participantFollowUpReceivingFurtherECPTx").setValue(participantFollowUpReceivingFurtherECPTx);
    }
    
    public String getParticipantAlive() {
        return (String) attributes.get("participantAlive").getValue();
    }
    
    public void setParticipantAlive(String participantFollowUpCauseDeath) {
        attributes.get("participantAlive").setValue(participantFollowUpCauseDeath);
    }
    
    public Date getParticipantFollowUpDateDeath() {
        return (Date) attributes.get("participantFollowUpDateDeath").getValue();
    }
    
    public void setParticipantFollowUpDateDeath(Date participantFollowUpDateDeath) {
        attributes.get("participantFollowUpDateDeath").setValue(participantFollowUpDateDeath);
    }
    
    public String getParticipantFollowUpCauseDeath() {
        return (String) attributes.get("participantFollowUpCauseDeath").getValue();
    }
    
    public void setParticipantFollowUpCauseDeath(String participantFollowUpCauseDeath) {
        attributes.get("participantFollowUpCauseDeath").setValue(participantFollowUpCauseDeath);
    }
    
    public String getParticipantFollowUpOtherCauseDeath() {
        return (String) attributes.get("participantFollowUpOtherCauseDeath").getValue();
    }
    
    public void setParticipantFollowUpOtherCauseDeath(String participantFollowUpCauseDeath) {
        attributes.get("participantFollowUpOtherCauseDeath").setValue(participantFollowUpCauseDeath);
    }
    
    public String getParticipantFollowUpReceivingPFTs() {
        return (String) attributes.get("participantFollowUpReceivingPFTs").getValue();
    }
    
    public void setParticipantFollowUpReceivingPFTs(String participantFollowUpReceivingPFTs) {
        attributes.get("participantFollowUpReceivingPFTs").setValue(participantFollowUpReceivingPFTs);
    }
    
    public Date getParticipantFollowUpECPTxDate1() {
        return (Date) attributes.get("participantFollowUpECPTxDate1").getValue();
    }
    
    public void setParticipantFollowUpECPTxDate1(Date participantFollowUpECPTxDate1) {
        attributes.get("participantFollowUpECPTxDate1").setValue(participantFollowUpECPTxDate1);
    }

    public Date getParticipantFollowUpECPTxDate2() {
        return (Date) attributes.get("participantFollowUpECPTxDate2").getValue();
    }
    
    public void setParticipantFollowUpECPTxDate2(Date participantFollowUpECPTxDate2) {
        attributes.get("participantFollowUpECPTxDate2").setValue(participantFollowUpECPTxDate2);
    }

    public Date getParticipantFollowUpECPTxDate3() {
        return (Date) attributes.get("participantFollowUpECPTxDate3").getValue();
    }
    
    public void setParticipantFollowUpECPTxDate3(Date participantFollowUpECPTxDate3) {
        attributes.get("participantFollowUpECPTxDate3").setValue(participantFollowUpECPTxDate3);
    }

    public Date getParticipantFollowUpECPTxDate4() {
        return (Date) attributes.get("participantFollowUpECPTxDate3").getValue();
    }
    
    public void setParticipantFollowUpECPTxDate4(Date participantFollowUpECPTxDate4) {
        attributes.get("participantFollowUpECPTxDate3").setValue(participantFollowUpECPTxDate4);
    }
    
    public Date getParticipantFollowUpECPTxDate5() {
        return (Date) attributes.get("participantFollowUpECPTxDate3").getValue();
    }
    
    public void setParticipantFollowUpECPTxDate5(Date participantFollowUpECPTxDate5) {
        attributes.get("participantFollowUpECPTxDate3").setValue(participantFollowUpECPTxDate5);
    }
    
    public Date getParticipantFollowUpECPTxDate6() {
        return (Date) attributes.get("participantFollowUpECPTxDate3").getValue();
    }
    
    public void setParticipantFollowUpECPTxDate6(Date participantFollowUpECPTxDate6) {
        attributes.get("participantFollowUpECPTxDate3").setValue(participantFollowUpECPTxDate6);
    }
    public VerificationStatus getParticipantFollowUpECPTxDate1VerificationStatus() {
        logger.audit("ParticipantFollowUpForm, getParticipantFollowUpECPTxDate1VerificationStatus");
        logger.audit("ParticipantFollowUpForm, getParticipantFollowUpECPTxDate1VerificationStatus" + attributes.get("participantFollowUpECPTxDate1").getVerificationStatus());

//        System.out.println("getTerminationReasonVerify was called containing: " + attributes.get("terminationReason").getVerificationStatus());
        return attributes.get("participantFollowUpECPTxDate1").getVerificationStatus();
    }
    
    public void setParticipantFollowUpECPTxDate1VerificationStatus(VerificationStatus verificationStatus) {
//        System.out.println("SET TerminationReasonVerify was called containing: " + verificationStatus);
        attributes.get("participantFollowUpECPTxDate1").setVerificationStatus(verificationStatus);
    }

    public String getParticipantFollowUpECPTxDate1DccComment() {
        return (String) attributes.get("participantFollowUpECPTxDate1").getDccComment();
    }
    
    public void setParticipantFollowUpECPTxDate1DccComment(String dccComment) {
        attributes.get("participantFollowUpECPTxDate1").setDccComment(dccComment);
    }

    public List<Date> getFollowUpTxDates() {
        return followUpECPTxDates;
    }

    public void initFollowUpTxDates() {
        String dateKey = "participantFollowUpECPTxDate";

        List<String> keys = new ArrayList<>(attributes.keySet());
        
        // Remove other attributes to count current number of TxDates
        List<String> keysToRemove = new ArrayList<>();
        
        for (String k : keys) {
            if (!k.startsWith(dateKey))
                keysToRemove.add(k);
        }
        
        logger.audit("Found non-date keys: " + keysToRemove.toString());
        
        keys.removeAll(keysToRemove);
        
        logger.audit("Remaining keys: " + keys.toString());
        
        // Copy all of the attributes into a list of dates
        List<Date> txDates = new ArrayList<>();
        Date d;
        
        for(int i = 0; i < keys.size(); i++){
            String attribute = dateKey + (i + 1);
            logger.audit("Processing " + attribute);
            
            d = (Date) attributes.get(attribute).getValue();
            txDates.add(d);
        }
        
        followUpECPTxDates = txDates;
    }
    
    public void saveFollowUpTxDates() {
        String dateKey = "participantFollowUpECPTxDate";
        
        // Sync list of dates back into attributes, creating attributes if they don't exist
        for (int i = 0; i < followUpECPTxDates.size(); ++i) {
            String attribute = dateKey + (i + 1);
            
            if (!attributes.containsKey(attribute)) {
                attributes.put(attribute, new AttributeDate(attribute));
            }
        
            attributes.get(attribute).setValue(followUpECPTxDates.get(i));
        }
    }
    
    public String moreTxDatesAction() {
        followUpECPTxDates.add(null);
        followUpECPTxDates.add(null);
        followUpECPTxDates.add(null);
        
        // Stay on same page
        return null;
    }
}
